#ifndef QUAD_H
#define QUAD_H

#include "graphinterface.h"

class Quad : public GraphPolygon
{
private:
    static int const edges = 4;
public:
    Quad();
    bool isOver();
    void paintWith(QPainter &painter);
};
class QuadFactory : public QObject, ObjectFactory
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "plugin.GraphBase")
    Q_INTERFACES(ObjectFactory)
public:
    QuadFactory();
    GraphBase *create(QString const &thisType);
    QPushButton *createButton();

private slots:
    void changeShape();
};

#endif // QUAD_H
