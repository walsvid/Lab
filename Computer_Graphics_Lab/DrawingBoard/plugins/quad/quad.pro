TEMPLATE        = lib
CONFIG         += plugin
QT             += widgets
INCLUDEPATH    += ../../core
TARGET          = quad
DESTDIR         = ../../pluginDir

HEADERS += \
    quad.h

SOURCES += \
    quad.cpp

RESOURCES += \
    quad.qrc
