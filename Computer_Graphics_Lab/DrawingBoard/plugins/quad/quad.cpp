#include <QtWidgets>
#include "quad.h"

#define TYPE "Quad"

//Implement Quad
Quad::Quad()
{
    graphType = TYPE;
}

bool Quad::isOver()
{
    return points.size() == edges;
}

void Quad::paintWith(QPainter &painter)
{
    painter.drawPolygon(points);

    if (points.size() == edges)
    {
        painter.drawLine(points[edges -1], points[0]);
    }
    else if (points.size() == edges - 1)
    {
        painter.drawLine(points[points.size() - 1], tempPoint);
        painter.drawLine(tempPoint, points[0]);
    }
    else
    {
        painter.drawLine(points[points.size() - 1], tempPoint);
    }
}

QuadFactory::QuadFactory()
{

}

GraphBase *QuadFactory::create(QString const &thisType)
{
    if (thisType == TYPE)
        return new Quad;
    else
        return NULL;
}

QPushButton *QuadFactory::createButton()
{
    QPushButton *quadBtn = new QPushButton;
    quadBtn->setIcon(QIcon(":/icon/quad"));
    connect(quadBtn, SIGNAL(clicked(bool)), this, SLOT(changeShape()));
    return quadBtn;
}

void QuadFactory::changeShape()
{
    pCallCommand->execute(TYPE);
}
