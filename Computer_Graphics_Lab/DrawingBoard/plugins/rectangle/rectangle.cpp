#include <QtWidgets>
#include "rectangle.h"

#define TYPE "Rectangle"

//Rectangle
Rectangle::Rectangle()
{
    graphType = TYPE;
}

bool Rectangle::isContained(QPoint const &point)
{
    return QGraphicsRectItem(QRect(beginPoint, endPoint)).contains(QPointF(point));
}

void Rectangle::paintWith(QPainter &painter)
{
    painter.drawRect(QRect(beginPoint, endPoint));
}

RectangleFactory::RectangleFactory()
{

}

GraphBase *RectangleFactory::create(QString const &thisType)
{
    if (thisType == TYPE)
        return new Rectangle;
    else
        return NULL;
}

QPushButton *RectangleFactory::createButton()
{
    QPushButton *rectangleBtn = new QPushButton;
    rectangleBtn->setIcon(QIcon(":/icon/rectangle"));
    connect(rectangleBtn, SIGNAL(clicked(bool)), this, SLOT(changeShape()));
    return rectangleBtn;
}

void RectangleFactory::changeShape()
{
    pCallCommand->execute(TYPE);
}
