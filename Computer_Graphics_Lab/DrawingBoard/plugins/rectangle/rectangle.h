#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "graphinterface.h"

class Rectangle : public GraphPane
{
public:
    Rectangle();
    bool isContained(QPoint const &point);
    void paintWith(QPainter &painter);
};

class RectangleFactory : public QObject, ObjectFactory
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "plugin.GraphBase")
    Q_INTERFACES(ObjectFactory)
public:
    RectangleFactory();
    GraphBase *create(QString const &thisType);
    QPushButton *createButton();

private slots:
    void changeShape();
};

#endif //RECTANGLE_H
