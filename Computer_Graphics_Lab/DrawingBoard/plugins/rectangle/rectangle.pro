TEMPLATE        = lib
CONFIG         += plugin
QT             += widgets
INCLUDEPATH    += ../../core
TARGET          = rectangle
DESTDIR         = ../../pluginDir
HEADERS         += \
                rectangle.h
SOURCES         += \
                rectangle.cpp

RESOURCES += \
    rectangle.qrc
