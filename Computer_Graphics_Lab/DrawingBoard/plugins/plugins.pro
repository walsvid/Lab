TEMPLATE    = subdirs
SUBDIRS     = doodle \
            circle \
            rectangle \
            ellipse \
            line \
            triangle \
            npoly \
            quad \
            bezier
