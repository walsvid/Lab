TEMPLATE        = lib
CONFIG         += plugin
QT             += widgets
INCLUDEPATH    += ../../core
TARGET          = triangle
DESTDIR         = ../../pluginDir
HEADERS         += \
                triangle.h
SOURCES         += \
                triangle.cpp

RESOURCES += \
    triangle.qrc
