#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "graphinterface.h"

class Triangle : public GraphPolygon
{
private:
     static int const edges = 3;
public:
    Triangle();
    bool isOver();
    void paintWith(QPainter &painter);
};

class TriangleFactory : public QObject, ObjectFactory
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "plugin.GraphBase")
    Q_INTERFACES(ObjectFactory)
public:
    TriangleFactory();
    GraphBase *create(QString const &thisType);
    QPushButton *createButton();

private slots:
    void changeShape();
};

#endif //TRIANGLE_H
