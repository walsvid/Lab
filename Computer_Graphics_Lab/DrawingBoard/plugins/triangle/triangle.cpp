#include <QtWidgets>
#include "triangle.h"

#define TYPE "Triangle"

//Implement Triangle
Triangle::Triangle()
{
    graphType = TYPE;
}

bool Triangle::isOver()
{
    return points.size() == edges;
}

void Triangle::paintWith(QPainter &painter)
{
    painter.drawPolygon(points);

    if (points.size() == edges)
    {
        painter.drawLine(points[edges -1], points[0]);
    }
    else
    {
        painter.drawLine(points[points.size() - 1], tempPoint);
        painter.drawLine(tempPoint, points[0]);
    }
}

TriangleFactory::TriangleFactory()
{

}

GraphBase *TriangleFactory::create(QString const &thisType)
{
    if (thisType == TYPE)
        return new Triangle;
    else
        return NULL;
}

QPushButton *TriangleFactory::createButton()
{
    QPushButton *triangleBtn = new QPushButton;
    triangleBtn->setIcon(QIcon(":/icon/triangle"));
    connect(triangleBtn, SIGNAL(clicked(bool)), this, SLOT(changeShape()));
    return triangleBtn;
}

void TriangleFactory::changeShape()
{
    pCallCommand->execute(TYPE);
}
