TEMPLATE        = lib
CONFIG         += plugin
QT             += widgets
INCLUDEPATH    += ../../core
TARGET          = bezier
DESTDIR         = ../../pluginDir

HEADERS += \
    bezier.h

SOURCES += \
    bezier.cpp

RESOURCES += \
    bezier.qrc
