#ifndef BEZIER_H
#define BEZIER_H

#include "graphinterface.h"

class Bezier : public GraphCurve
{
public:
    Bezier();
    void paintWith(QPainter &painter);
};

class BezierFactory : public QObject, ObjectFactory
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "plugin.GraphBase")
    Q_INTERFACES(ObjectFactory)
public:
    BezierFactory();
    GraphBase *create(QString const &thisType);
    QPushButton *createButton();

private slots:
    void changeShape();
};

#endif // BEZIER_H
