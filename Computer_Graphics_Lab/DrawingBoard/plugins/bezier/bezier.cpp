#include <QtWidgets>
#include "bezier.h"

#define TYPE "Bezier"

//Bezier
Bezier::Bezier()
{
    graphType = TYPE;
}

void Bezier::paintWith(QPainter &painter)
{
    ctrlPt1 = (beginPoint + QPoint(50,50));
    ctrlPt2 = (endPoint + QPoint(50, 50));
    painter.setPen(Qt::black);
    painter.drawLine(beginPoint, ctrlPt1);
    painter.drawLine(ctrlPt2, endPoint);
    painter.setPen(QPen(QColor(255, 0, 0), 2));
    QPainterPath bezierPath(beginPoint);
    bezierPath.cubicTo(ctrlPt1, ctrlPt2, endPoint);
    painter.drawPath(bezierPath);
    painter.setBrush(QColor(255, 0, 0));
    painter.drawEllipse(ctrlPt1, 3, 3);
    painter.drawEllipse(ctrlPt2, 3, 3);
    painter.drawEllipse(beginPoint, 3, 3);
    painter.drawEllipse(endPoint, 3, 3);
}

BezierFactory::BezierFactory()
{

}

GraphBase *BezierFactory::create(QString const &thisType)
{
    if (thisType == TYPE)
        return new Bezier;
    else
        return NULL;
}

QPushButton *BezierFactory::createButton()
{
    QPushButton *bezierBtn = new QPushButton;
    bezierBtn->setIcon(QIcon(":/icon/bezier"));
    connect(bezierBtn, SIGNAL(clicked(bool)), this, SLOT(changeShape()));
    return bezierBtn;
}

void BezierFactory::changeShape()
{
    pCallCommand->execute(TYPE);
}
