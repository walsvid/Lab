#include <QtWidgets>
#include "doodle.h"

#define TYPE "GraphDoodle"

//Implement GraphDoole
GraphDoodle::GraphDoodle() : drawingCanvas(QSize(1000, 1000), QImage::Format_ARGB32)
{
    graphType = TYPE;
    drawingCanvas.fill(Qt::transparent);
}

void GraphDoodle::drawBegin(QPoint const &point)
{
    lastPoint = point;
}

void GraphDoodle::drawEnd(MouseStatus status, const QPoint &point)
{
    QPainter painter(&drawingCanvas);
    painter.drawLine(point, lastPoint);
    lastPoint = point;
}

void GraphDoodle::paintWith(QPainter &painter)
{
    painter.drawImage(QPoint(0,0), drawingCanvas);
}

void GraphDoodle::wirteToStream(QDataStream &out)
{
    out << drawingCanvas;
}

void GraphDoodle::readFromStream(QDataStream &in)
{
    in >> drawingCanvas;
}

bool GraphDoodle::isMoveable()
{
    return false;
}

bool GraphDoodle::isOver()
{
    return true;
}

GraphDoodleFactory::GraphDoodleFactory()
{

}

GraphBase *GraphDoodleFactory::create(QString const &thisType)
{
    if (thisType == TYPE)
        return new GraphDoodle;
    else
        return NULL;
}

QPushButton *GraphDoodleFactory::createButton()
{
    QPushButton *doodleBtn = new QPushButton;
    doodleBtn->setIcon(QIcon(":/icon/doodle"));
    connect(doodleBtn, SIGNAL(clicked(bool)), this, SLOT(changeShape()));
    return doodleBtn;
}

void GraphDoodleFactory::changeShape()
{
    pCallCommand->execute(TYPE);
}
