#ifndef DOODLE_H
#define DOODLE_H

#include "graphinterface.h"

class GraphDoodle : public GraphBase
{
private:
    QImage drawingCanvas;
    QPoint lastPoint;

public:
    GraphDoodle();

    void drawBegin(QPoint const &point);
    void drawEnd(MouseStatus status, QPoint const &point);

    void paintWith(QPainter &painter);

    void readFromStream(QDataStream &in);
    void wirteToStream(QDataStream &out);

    bool isMoveable();
    bool isOver();
};

class GraphDoodleFactory : public QObject, ObjectFactory
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "plugin.GraphBase")
    Q_INTERFACES(ObjectFactory)
public:
    GraphDoodleFactory();
    GraphBase *create(QString const &thisType);
    QPushButton *createButton();

private slots:
    void changeShape();
};

#endif //DOODLE_H
