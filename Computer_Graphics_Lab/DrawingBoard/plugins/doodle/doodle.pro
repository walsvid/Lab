TEMPLATE        = lib
CONFIG         += plugin
QT             += widgets
INCLUDEPATH    += ../../core
TARGET          = doodle
DESTDIR         = ../../pluginDir
HEADERS         += \
                doodle.h
SOURCES         += \
                doodle.cpp

RESOURCES += \
    doodle.qrc
