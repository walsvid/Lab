TEMPLATE        = lib
CONFIG         += plugin
QT             += widgets
INCLUDEPATH    += ../../core
TARGET          = circle
DESTDIR         = ../../pluginDir
HEADERS         += \
                circle.h
SOURCES         += \
                circle.cpp

RESOURCES += \
    circle.qrc
