#include <QtWidgets>
#include "circle.h"

#define TYPE "Circle"
//Circle
Circle::Circle()
{
    graphType = TYPE;
}

bool Circle::isContained(QPoint const &point)
{
    int diameter = endPoint.x() - beginPoint.x();
    return QGraphicsEllipseItem(beginPoint.x(), beginPoint.y(),
                                diameter, diameter).contains(QPointF(point));
}

void Circle::paintWith(QPainter &painter)
{
    int diameter = endPoint.x() - beginPoint.x();
    painter.drawEllipse(beginPoint.x(), beginPoint.y(), diameter, diameter);
}

CircleFactory::CircleFactory()
{

}

GraphBase *CircleFactory::create(QString const &thisType)
{
    if (thisType == TYPE)
        return new Circle;
    else
        return NULL;
}

QPushButton *CircleFactory::createButton()
{
    QPushButton *circleBtn = new QPushButton;
    circleBtn->setIcon(QIcon(":/icon/circle"));
    connect(circleBtn, SIGNAL(clicked(bool)), this, SLOT(changeShape()));
    return circleBtn;
}

void CircleFactory::changeShape()
{
    pCallCommand->execute(TYPE);
}
