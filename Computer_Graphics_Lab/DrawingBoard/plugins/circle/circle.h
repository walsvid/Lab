#ifndef CIRCLE_H
#define CIRCLE_H

#include "graphinterface.h"

class Circle : public GraphPane
{
public:
    Circle();
    bool isContained(QPoint const &point);
    void paintWith(QPainter &painter);
};

class CircleFactory : public QObject, ObjectFactory
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "plugin.GraphBase")
    Q_INTERFACES(ObjectFactory)
public:
    CircleFactory();
    GraphBase *create(QString const &thisType);
    QPushButton *createButton();

private slots:
    void changeShape();
};

#endif //CIRCLE_H
