#ifndef NPOLY_H
#define NPOLY_H

#include "graphinterface.h"

class Npoly : public GraphPolygon
{
private:
    bool closeEnought();
public:
    Npoly();
    bool isOver();
    void paintWith(QPainter &painter);
};
class NpolyFactory : public QObject, ObjectFactory
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "plugin.GraphBase")
    Q_INTERFACES(ObjectFactory)
public:
    NpolyFactory();
    GraphBase *create(QString const &thisType);
    QPushButton *createButton();

private slots:
    void changeShape();
};

#endif // NPOLY_H
