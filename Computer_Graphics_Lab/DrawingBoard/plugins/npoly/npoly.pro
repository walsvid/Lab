TEMPLATE        = lib
CONFIG         += plugin
QT             += widgets
INCLUDEPATH    += ../../core
TARGET          = npoly
DESTDIR         = ../../pluginDir

HEADERS += \
    npoly.h

SOURCES += \
    npoly.cpp

RESOURCES += \
    npoly.qrc
