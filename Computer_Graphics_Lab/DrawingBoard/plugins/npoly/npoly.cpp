#include <QtWidgets>
#include "npoly.h"

#define TYPE "Npoly"

//Implement Npoly
Npoly::Npoly()
{
    graphType = TYPE;
}

bool Npoly::closeEnought()
{
    QPoint dist = points[points.size() - 1] - points[0];
    return dist.manhattanLength() < 5;
}

bool Npoly::isOver()
{
    return closeEnought();
}

void Npoly::paintWith(QPainter &painter)
{
    painter.drawPolygon(points);
    if (closeEnought())
    {
        painter.drawLine(points[points.size() - 1], points[0]);
    }
    else
    {
        painter.drawLine(points[points.size() - 1], tempPoint);
        painter.drawLine(tempPoint, points[0]);
    }
}

NpolyFactory::NpolyFactory()
{

}

GraphBase *NpolyFactory::create(QString const &thisType)
{
    if (thisType == TYPE)
        return new Npoly;
    else
        return NULL;
}

QPushButton *NpolyFactory::createButton()
{
    QPushButton *npolyBtn = new QPushButton;
    npolyBtn->setIcon(QIcon(":/icon/npoly"));
    connect(npolyBtn, SIGNAL(clicked(bool)), this, SLOT(changeShape()));
    return npolyBtn;
}

void NpolyFactory::changeShape()
{
    pCallCommand->execute(TYPE);
}
