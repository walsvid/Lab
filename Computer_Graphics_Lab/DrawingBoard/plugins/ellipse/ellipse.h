#ifndef ELLIPSE_H
#define ELLIPSE_H

#include "graphinterface.h"

class Ellipse : public GraphPane
{
public:
    Ellipse();
    bool isContained(QPoint const &point);
    void paintWith(QPainter &painter);
};

class EllipseFactory : public QObject, ObjectFactory
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "plugin.GraphBase")
    Q_INTERFACES(ObjectFactory)
public:
    EllipseFactory();
    GraphBase *create(QString const &thisType);
    QPushButton *createButton();

private slots:
    void changeShape();
};

#endif //ELLIPSE_H
