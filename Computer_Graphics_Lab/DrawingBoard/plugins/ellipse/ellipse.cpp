#include <QtWidgets>
#include "ellipse.h"


#define TYPE "Ellipse"

//Ellipse
Ellipse::Ellipse()
{
    graphType = TYPE;
}

bool Ellipse::isContained(QPoint const &point)
{
    return QGraphicsEllipseItem(beginPoint.x(), beginPoint.y(),
                                endPoint.x() - beginPoint.x(),
                                endPoint.y() - beginPoint.y()).contains(QPointF(point));
}

void Ellipse::paintWith(QPainter &painter)
{
    painter.drawEllipse(beginPoint.x(), beginPoint.y(),
                        endPoint.x() - beginPoint.x(),
                        endPoint.y() - beginPoint.y());
}

EllipseFactory::EllipseFactory()
{

}

GraphBase *EllipseFactory::create(QString const &thisType)
{
    if (thisType == TYPE)
        return new Ellipse;
    else
        return NULL;
}

QPushButton *EllipseFactory::createButton()
{
    QPushButton *ellipseBtn = new QPushButton;
    ellipseBtn->setIcon(QIcon(":/icon/ellipse"));
    connect(ellipseBtn, SIGNAL(clicked(bool)), this, SLOT(changeShape()));
    return ellipseBtn;
}

void EllipseFactory::changeShape()
{
    pCallCommand->execute(TYPE);
}
