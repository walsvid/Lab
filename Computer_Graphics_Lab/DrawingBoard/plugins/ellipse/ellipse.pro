TEMPLATE        = lib
CONFIG         += plugin
QT             += widgets
INCLUDEPATH    += ../../core
TARGET          = ellipse
DESTDIR         = ../../pluginDir
HEADERS         += \
                ellipse.h
SOURCES         += \
                ellipse.cpp

RESOURCES += \
    ellipse.qrc
