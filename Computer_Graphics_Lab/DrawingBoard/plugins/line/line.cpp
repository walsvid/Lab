#include <QtWidgets>
#include "line.h"


#define TYPE "Line"

//Line
Line::Line()
{
    graphType = TYPE;
}

bool Line::isContained(QPoint const &point)
{
    return false;
}

void Line::paintWith(QPainter &painter)
{
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.drawLine(beginPoint, endPoint);
}

LineFactory::LineFactory()
{

}

GraphBase *LineFactory::create(QString const &thisType)
{
    if (thisType == TYPE)
        return new Line;
    else
        return NULL;
}

QPushButton *LineFactory::createButton()
{
    QPushButton *lineBtn = new QPushButton;
    lineBtn->setIcon(QIcon(":/icon/line"));
    connect(lineBtn, SIGNAL(clicked(bool)), this, SLOT(changeShape()));
    return lineBtn;
}

void LineFactory::changeShape()
{
    pCallCommand->execute(TYPE);
}
