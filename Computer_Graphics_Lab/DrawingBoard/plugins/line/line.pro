TEMPLATE        = lib
CONFIG         += plugin
QT             += widgets
INCLUDEPATH    += ../../core
TARGET          = line
DESTDIR         = ../../pluginDir
HEADERS         += \
                line.h
SOURCES         += \
                line.cpp

RESOURCES += \
    line.qrc
