#ifndef LINE_H
#define LINE_H

#include "graphinterface.h"

class Line : public GraphPane
{
public:
    Line();
    bool isContained(QPoint const &point);
    void paintWith(QPainter &painter);
};

class LineFactory : public QObject, ObjectFactory
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "plugin.GraphBase")
    Q_INTERFACES(ObjectFactory)
public:
    LineFactory();
    GraphBase *create(QString const &thisType);
    QPushButton *createButton();

private slots:
    void changeShape();
};

#endif //LINE_H
