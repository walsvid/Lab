#ifndef GRAPHINTERFACE_H
#define GRAPHINTERFACE_H

#include <QWidget>
#include <QSize>
#include <QVector>
#include <QString>
#include <QImage>
#include <QPoint>
#include <QDataStream>
#include <QDebug>
#include <QPainter>
#include <QPushButton>
#include <QLabel>

enum MouseStatus {
    Press,
    Move,
    Release
};

enum CurrentOpt
{
    drawing,
    moving
};

// Define Command
class Board;
class Command
{
public:
    virtual ~Command() {}
    virtual void execute(QString const &graphName) = 0;
protected:
    Command() {}
};

class ChangeGraphCommand : public Command
{
public:
    ChangeGraphCommand(Board *thisBoard);

    void execute(QString const &graphName);

private:
    Board *_board;
};

// Define Graphs

class GraphBase
{
public:

    virtual void drawBegin(QPoint const &point) = 0;
    virtual void drawEnd(MouseStatus status, QPoint const &point) = 0;

    virtual void paintWith(QPainter &painter) = 0;

    virtual void readFromStream(QDataStream &in) = 0;
    virtual void wirteToStream(QDataStream &out) = 0;

    virtual bool isMoveable() = 0;
    virtual bool isOver() = 0;

    QString const &getType()
    {
        return graphType;
    }
protected:
    QString graphType;
};

class GraphArea : public GraphBase
{
protected:
    QPoint moveBeginpoint;
    QPoint moveDirection;
public:
    virtual void moveEnd(QPoint const &point) = 0;
    virtual bool isContained(QPoint const &point) = 0;

    void moveBegin(QPoint const &point)
    {
        moveBeginpoint = point;
    }

    bool isMoveable()
    {
        return true;
    }
};

class GraphPolygon : public GraphArea
{
protected:
    QVector<QPoint> points;
    QPoint tempPoint;
public:
    void drawBegin(QPoint const &point)
    {
        points.push_back(point);
    }

    void drawEnd(MouseStatus status, QPoint const &point)
    {
        if(status == Release)
        {
            points.push_back(point);
        }
        else
        {
            tempPoint = point;
        }
    }

    void paintWith(QPainter &painter)
    {
        painter.drawPolygon(QPolygon(points));
    }

    void readFromStream(QDataStream &in)
    {
        int size;
        in >> size;
        points.resize(size);
        for(int i = 0; i < points.size(); i++)
        {
            in >> points[i];
            qDebug() << points[i];
        }
    }
    void wirteToStream(QDataStream &out)
    {
        out << points.size();
        for(int i = 0; i < points.size(); i++)
        {
            out << points[i];
            qDebug() << points[i];
        }
    }

    void moveEnd(QPoint const &point)
    {
        moveDirection = point - moveBeginpoint;
        for(int i = 0; i < points.size(); i++)
        {
            points[i] += moveDirection;
        }
        moveBeginpoint = point;
    }

    bool isContained(QPoint const &point)
    {
        return QPolygon(points).containsPoint(point, Qt::OddEvenFill);
    }

};

class GraphPane : public GraphArea
{
protected:
    QPoint beginPoint;
    QPoint endPoint;
public:
    void drawBegin(QPoint const &point)
    {
        beginPoint = point;
    }
    void drawEnd(MouseStatus status, QPoint const &point)
    {
        endPoint = point;
    }

    void readFromStream(QDataStream &in)
    {
        in >> beginPoint >> endPoint;
    }

    void wirteToStream(QDataStream &out)
    {
        out << beginPoint << endPoint;
    }

    void moveBegin(QPoint const &point);
    void moveEnd(QPoint const &point)
    {
        moveDirection = point - moveBeginpoint;
        beginPoint += moveDirection;
        endPoint += moveDirection;
        moveBeginpoint = point;
    }

    bool isOver()
    {
        return true;
    }
};

class GraphCurve : public GraphBase
{
protected:
    QPoint ctrlPt1;
    QPoint ctrlPt2;
    QPoint beginPoint;
    QPoint endPoint;
public:
    void drawBegin(QPoint const &point)
    {
        beginPoint = point;
    }

    void drawEnd(MouseStatus status, QPoint const &point)
    {
        endPoint = point;
    }
    void readFromStream(QDataStream &in)
    {
        in >> beginPoint >> endPoint;
    }

    void wirteToStream(QDataStream &out)
    {
        out << beginPoint << endPoint;
    }

    bool isMoveable()
    {
        return true;
    }

    bool isOver()
    {
        return true;
    }
};

class ObjectFactory
{
public:
    virtual GraphBase *create(QString const &thisType) = 0;
    virtual QPushButton *createButton() = 0;

    void callCommand(Command *p)
    {
        pCallCommand = p;
    }
protected:
    Command *pCallCommand;
};
Q_DECLARE_INTERFACE(ObjectFactory, "plugin.GraphBase")

#endif // GRAPHINTERFACE_H
