#ifndef BOARD_H
#define BOARD_H

#include <QDataStream>
#include <QWidget>
#include <QSize>
#include <QList>
#include <QPointer>
#include <QString>
#include <QImage>
#include <QPoint>
#include <QDir>
#include <QPluginLoader>
#include "graphinterface.h"



class pluginFactory : public QObject
{
public:
    pluginFactory();
    void loadPlugin(QDir const &pluginsPath);
    GraphBase *getPlugin(QString const &pluginType);
    QVector<QPushButton *> getPluginButton(Command *p);
    ~pluginFactory();

private:
    QVector<QPluginLoader *> pluginLoader;
};

class Board : public QWidget
{
    Q_OBJECT
public:
    Board();
    ~Board();

    bool isModified();
    void setModified(bool status);

    void loadFile(QString const &filename);
    void saveFile(QString const &filename);
    void saveAs(QString const &filename, QByteArray format);

    void receiveShapeChange(QString const &graphType);

    QByteArray const &getSuffix();
    QSize const getsize();

    QVector<QPushButton *> getPluginButton();

signals:
    void sizeChanging(const QSize &);
    void redoAvailable(bool isAvailable);
    bool undoAvailable(bool isAvailable);

private slots:
//    void setDoodle();

//    void setRect();
//    void setCircle();
//    void setLine();
//    void setEllipse();

//    void setTriangle();

    void setMove();

    void redo();
    void undo();

    void resizeCanvas(QSize const &size);

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *);

private:
    void loadPlugin();

    void setDrawingMode(QPoint const &point);
    void setSelectMode(QPoint const &point);

    void loadImage(QString const &filename);
    void loadMyFmt(QString const &filename);

    QImage canvas, background;
    QString currentGraphType;
    pluginFactory factory;

    QList<GraphBase *> elementsAll;
    QList<GraphBase *> elementsBuffer;

    CurrentOpt curOpt;

    GraphArea *currentMovingObj;
    GraphBase *currentDrawingObj;

    bool isBeingModified;

    QByteArray fileSuffix;
};

#endif // BOARD_H
