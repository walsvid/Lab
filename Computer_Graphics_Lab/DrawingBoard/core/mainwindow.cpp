#include <QtWidgets>
#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    board = new Board;
    setCentralWidget(board);

    resize(board->getsize().width(),
           board->getsize().height() + 150);

    createAction();
    createButton();
    createMenu();
    createToolBar();
    createDockWindow();
}

MainWindow::~MainWindow()
{

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (questionAndSave())
        event->accept();
    else
        event->ignore();
}

void MainWindow::createMenu()
{
    fileMenu = menuBar()->addMenu(tr("&file"));
    fileMenu->addAction(openAct);
    fileMenu->addSeparator();
    fileMenu->addAction(saveAct);

    editMenu = menuBar()->addMenu(tr("&edit"));
    editMenu->addAction(undoAct);
    editMenu->addAction(redoAct);
}

void MainWindow::createDockWindow()
{
    //set graphs
    QVBoxLayout *graphLayout = new QVBoxLayout;
    QVector<QPushButton *> allButton = board->getPluginButton();
    foreach(QPushButton *button, allButton)
    {
        graphLayout->addWidget(button);
    }

    graphs = new QGroupBox(tr("graph"));
    //setLayout(graphLayout);
    graphs->setLayout(graphLayout);

    //set tools
    QVBoxLayout *toolLayout = new QVBoxLayout;
    setLayout(toolLayout);
    toolLayout->addWidget(moveBtn, 0, 0);
    tools = new QGroupBox(tr("tool"));
    tools->setLayout(toolLayout);

    QVBoxLayout *paintLayout = new QVBoxLayout;
    paintLayout->addWidget(graphs, 0, 0);
    paintLayout->addWidget(tools, 1, 0);
    setLayout(paintLayout);
    paintBox = new QGroupBox;
    paintBox->setLayout(paintLayout);

    BoxDock = new QDockWidget(tr("Box"));
    BoxDock->setFeatures(QDockWidget::NoDockWidgetFeatures);
    addDockWidget(Qt::LeftDockWidgetArea, BoxDock);

    BoxDock->setWidget(paintBox);
}

void MainWindow::setLayout(QVBoxLayout *layout)
{
    layout->setAlignment(Qt::AlignLeft);
    layout->setSpacing(1);
    layout->setContentsMargins(1, 1, 1, 1);
}

bool MainWindow::questionAndSave()
{
    if (board->isModified())
    {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("painter"),
                                   tr("The image has been modified.\n"
                                      "Do you want to save your changes?"),
                                     QMessageBox::Save
                                   | QMessageBox::Discard
                                   | QMessageBox::Cancel);
        if (ret == QMessageBox::Save)
            return save();
        else if (ret == QMessageBox::Cancel)
            return false;
        else
            return true;
    }
    else
    {
        return true;
    }
}

void MainWindow::createAction()
{
    openAct = new QAction(QIcon(":/tools/open"), tr("&open"), this);
    openAct->setShortcut(QKeySequence::Open);
    connect(openAct, SIGNAL(triggered(bool)), this, SLOT(open()));

    saveAct = new QAction(QIcon(":/tools/save"), tr("&save"), this);
    saveAct->setShortcut(QKeySequence::Save);
    connect(saveAct, SIGNAL(triggered(bool)), this, SLOT(save()));

    undoAct = new QAction(QIcon(":/tools/undo"), tr("&undo"), this);
    undoAct->setShortcut(QKeySequence::Undo);
    connect(undoAct, SIGNAL(triggered(bool)), board, SLOT(undo()));
    connect(board, SIGNAL(undoAvailable(bool)), undoAct, SLOT(setEnabled(bool)));
    undoAct->setEnabled(false);

    redoAct = new QAction(QIcon(":/tools/redo"), tr("&redo"), this);
    redoAct->setShortcut(QKeySequence::Redo);
    connect(redoAct, SIGNAL(triggered(bool)), board, SLOT(redo()));
    connect(board, SIGNAL(redoAvailable(bool)), redoAct, SLOT(setEnabled(bool)));
    redoAct->setEnabled(false);
}

void MainWindow::createButton()
{
    moveBtn = new QPushButton(QIcon(":/graphs/move"), tr("move"), this);
    connect(moveBtn, SIGNAL(clicked(bool)), board, SLOT(setMove()));
    moveBtn->setAutoExclusive(true);
}

void MainWindow::createToolBar()
{
    fileToolBar = addToolBar("&file");
    editToolBar = addToolBar("&edit");
}

void MainWindow::open()
{
    if (questionAndSave())
    {
        currentfileName = QFileDialog::getOpenFileName(this, tr("open file"));
        if (!currentfileName.isEmpty())
        {
            board->loadFile(currentfileName);
        }
    }

    resize(board->getsize().width(),
           board->getsize().height() + 150);
}

bool MainWindow::save()
{
    if (currentfileName.isEmpty())
    {
        QString setDir = QDir::currentPath() + "/untitle." + board->getSuffix();
        currentfileName = QFileDialog::getSaveFileName(this,
                                                       tr("save file"),
                                                       setDir,
                                                       tr("Drawing Board(*.%1)").arg(board->getSuffix().constData()));
        if (currentfileName.isEmpty())
            return false;
    }
    if (board->isModified())
    {
        board->saveFile(currentfileName);
        board->setModified(false);
    }
    return true;
}
