#-------------------------------------------------
#
# Project created by QtCreator 2016-05-06T19:17:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DrawingBoard
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    board.cpp

HEADERS  += mainwindow.h \
    board.h \
    graphinterface.h

RESOURCES += \
    icon.qrc
