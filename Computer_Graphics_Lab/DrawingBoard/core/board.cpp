#include <QtWidgets>
#include "board.h"
#include "graphinterface.h"

Board::Board() : currentMovingObj(NULL), currentDrawingObj(NULL), isBeingModified(false)
{
    resizeCanvas(QSize(600, 300));
    fileSuffix.append("drb");
    QDir path = qApp->applicationDirPath();
    path.cdUp();
    path.cdUp();
    path.cdUp();
    path.cdUp();
    path.cd("pluginDir");
    factory.loadPlugin(path);
    currentGraphType = "Circle";

    setMouseTracking(true);
}

Board::~Board() {}

bool Board::isModified()
{
    return isBeingModified;
}

void Board::setModified(bool status)
{
    isBeingModified = status;
}

void Board::loadFile(QString const &filename)
{
    elementsAll.clear();
    QFileInfo fileInfo(filename);
    if (fileInfo.suffix() == fileSuffix)
        loadMyFmt(filename);
    else
        loadImage(filename);
}

void Board::saveFile(QString const &filename)
{
    QFile file(filename);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);

    out.setVersion(QDataStream::Qt_5_6);
    out << (quint32)0xAABBCCDD;

    out << canvas << background;
    foreach (GraphBase *iter, elementsAll)
    {
        out << iter->getType();
        qDebug() << iter->getType();
        iter->wirteToStream(out);
    }
}

void Board::saveAs(QString const &filename, QByteArray format)
{
    canvas.save(filename, format.constData());
}

QByteArray const &Board::getSuffix()
{
    return fileSuffix;
}

QSize const Board::getsize()
{
    return canvas.size();
}

QVector<QPushButton *> Board::getPluginButton()
{
    return factory.getPluginButton(new ChangeGraphCommand(this));
}

void Board::setMove()
{
    curOpt = moving;
    setCursor(Qt::SizeAllCursor);
}

void Board::receiveShapeChange(QString const &graphType)
{
    curOpt = drawing;
    currentGraphType = graphType;
    setCursor(Qt::CrossCursor);
}

void Board::redo()
{
    if (!elementsBuffer.isEmpty())
    {
        elementsAll.append(elementsBuffer.back());
        elementsBuffer.pop_back();
        emit undoAvailable(true);
        update();

        if (!elementsBuffer.isEmpty())
        {
            emit redoAvailable(true);
        }
        else
        {
            emit redoAvailable(false);
        }
    }
    else
    {
        emit redoAvailable(false);
    }
}

void Board::undo()
{
    if (!elementsAll.isEmpty())
    {
        elementsBuffer.append(elementsAll.back());
        elementsAll.pop_back();
        emit redoAvailable(true);
        update();

        if (!elementsAll.isEmpty())
        {
            emit undoAvailable(true);

        }
        else
        {
            emit undoAvailable(false);
        }
    }
    else
    {
        emit undoAvailable(false);
    }
}

void Board::resizeCanvas(QSize const &size)
{
       QImage image(size, QImage::Format_ARGB32);
       image.fill(Qt::transparent);
       QPainter painter(&image);
       painter.drawImage(QPoint(0,0), canvas);
       canvas = image;

       emit sizeChanging(size);
}

void Board::mousePressEvent(QMouseEvent *event)
{
    switch (curOpt) {
    case drawing:
        if (!currentDrawingObj && !currentGraphType.isEmpty())
            setDrawingMode(event->pos());
        break;
    case moving:
        setSelectMode(event->pos());
        break;
    default:
        break;
    }
    isBeingModified = true;
}

void Board::mouseReleaseEvent(QMouseEvent *event)
{
    switch (curOpt) {
    case drawing:
        if (currentDrawingObj)
        {
            currentDrawingObj->drawEnd(Release, event->pos());
            update();
            if (currentDrawingObj->isOver())
            {
                elementsAll.append(currentDrawingObj);
                currentDrawingObj = NULL;
            }
        }
        break;
    case moving:
        if (currentMovingObj)
        {
            currentMovingObj->moveEnd(event->pos());
            update();
            elementsAll.append(dynamic_cast<GraphBase *>(currentMovingObj));
            currentMovingObj = NULL;
        }
        break;
    default:
        break;
    }
}

void Board::mouseMoveEvent(QMouseEvent *event)
{
    switch (curOpt) {
    case drawing:
        if (currentDrawingObj)
        {
            currentDrawingObj->drawEnd(Move, event->pos());
            update();
        }
        break;
    case moving:
        if (currentMovingObj)
        {
            currentMovingObj->moveEnd(event->pos());
            update();
        }
        break;
    default:
        break;
    }
}

void Board::paintEvent(QPaintEvent *)
{
    canvas.fill(Qt::white);
    QPainter painter(&canvas);
    painter.drawImage(QPoint(0,0), background);

    foreach(GraphBase *iter, elementsAll)
        iter->paintWith(painter);
    if (currentDrawingObj)
        currentDrawingObj->paintWith(painter);
    if (currentMovingObj)
        currentMovingObj->paintWith(painter);

    QPainter paint(this);
    paint.drawImage(QPoint(0,0), canvas);

}

void Board::setDrawingMode(QPoint const &point)
{
    currentDrawingObj = factory.getPlugin(currentGraphType);
    currentDrawingObj->drawBegin(point);
    emit undoAvailable(true);
}

void Board::setSelectMode(QPoint const &point)
{
    currentMovingObj = NULL;
    foreach(GraphBase *iter, elementsAll)
    {
        if (iter->isMoveable())
        {
            currentMovingObj = dynamic_cast<GraphArea *>(iter);
            if (currentMovingObj->isContained(point))
            {
                currentMovingObj->moveBegin(point);
                elementsAll.removeOne(iter);
                break;
            }
            else
            {
                currentMovingObj = NULL;
            }
        }
    }
}

void Board::loadImage(QString const &filename)
{
    background.load(filename);
}

void Board::loadMyFmt(QString const &filename)
{
    QFile file(filename);
    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_5_6);

    quint32 flag;
    in >> flag;
    if (flag != 0xAABBCCDD)
        return;

    in >> canvas >> background;

    while (!in.atEnd())
    {
        QString type;
        in >> type;
        GraphBase *obj = factory.getPlugin(type);
        obj->readFromStream(in);
        elementsAll.append(obj);
    }

    update();
}


/***********************pluginFactory*********************/


pluginFactory::pluginFactory()
{

}

void pluginFactory::loadPlugin(QDir const &pluginsPath)
{
    foreach (QString filename, pluginsPath.entryList(QDir::Files))
    {
        pluginLoader.push_back(new QPluginLoader(pluginsPath.absoluteFilePath(filename)));
    }
}

GraphBase *pluginFactory::getPlugin(QString const &pluginType)
{
    foreach (QPluginLoader *loader, pluginLoader)
    {
        ObjectFactory *pFactory = qobject_cast<ObjectFactory *>(loader->instance());
        GraphBase *ret = pFactory->create(pluginType);
        if (ret)
            return ret;
    }
    return NULL;
}

QVector<QPushButton *> pluginFactory::getPluginButton(Command *p)
{
    QVector<QPushButton *> ret;
    foreach(QPluginLoader *loader, pluginLoader)
    {
        ObjectFactory *factory = qobject_cast<ObjectFactory *>(loader->instance());
        factory->callCommand(p);
        ret.push_back(factory->createButton());
    }
    return ret;
}

pluginFactory::~pluginFactory()
{
    foreach (QPluginLoader *loader, pluginLoader) {
        delete loader;
    }
}

ChangeGraphCommand::ChangeGraphCommand(Board *thisBoard)  : _board(thisBoard) {}

void ChangeGraphCommand::execute(QString const &graphName)
{
    _board->receiveShapeChange(graphName);
}
