/*
 * This head file contains essential data, declaration of functions
 *
 * Created by Wen Chao
 *
 * 2016-04-19
 *
 * Just for Computer Graphic Lab 01
 *
 * */

#ifndef CGLAB01_LAB01_H
#define CGLAB01_LAB01_H

#include <glut/glut.h>
#include <stdlib.h>

/*******************************Data declaration**********************************************/
GLsizei global_w, global_h;     // Set global window size
GLint dis_trans = 0;
GLdouble scale_x= 1.0;
GLdouble scale_y = 1.0;
GLdouble scale_z = 1.0;
GLint axis = 10;
GLfloat theta[3];
GLdouble vertices[][3] = { {-1.0, -1.0,  1.0}, {-1.0,  1.0,  1.0},
                          { 1.0,  1.0,  1.0}, { 1.0, -1.0,  1.0},
                          {-1.0, -1.0, -1.0}, {-1.0,  1.0, -1.0},
                          { 1.0,  1.0, -1.0}, { 1.0, -1.0, -1.0} };
//Useless
GLdouble colors[][3] = { { 1.0, 0.0, 0.0 }, { 0.0, 1.0, 1.0 },
                        { 1.0, 1.0, 0.0 }, { 0.0, 1.0, 0.0 },
                        { 0.0, 0.0, 1.0 }, { 1.0, 0.0, 1.0} };

/*******************************Function declaration**********************************************/
void polygon(GLint a, GLint b, GLint c, GLint d);

void cube();

void spinCube();

void myDisplay(void);

void myReshape(int w, int h);

void myKeyboard(unsigned char key, int x, int y);

void init(void);

#endif //CGLAB01_LAB01_H
