/*
 * This Project is a basic computer graphic lab.
 *
 * Aim: 0. Draw an object using functions in OpenGL, glut* is forbidden.
 *
 *      1. Implement a 3D object spin, translation and scale.
 *
 *      2. Setting materials and lighting.
 *
 *      3. Using the keyboard callback function.
 *
 * Created by Wen Chao
 *
 * 2016-04-19
 *
 * */

#include "lab01.h"
#include "material.h"
#include "light.h"

void polygon(GLint a, GLint b, GLint c, GLint d)
{
    glBegin(GL_POLYGON);
        glVertex3dv(vertices[a]);
        glVertex3dv(vertices[b]);
        glVertex3dv(vertices[c]);
        glVertex3dv(vertices[d]);
    glEnd();
}

void cube()
{
    glColor3dv(colors[0]);
//    set_materials(&brassMaterials);
//    glNormal3fv(cubeNomals[0]);
    polygon(0, 3, 2, 1);
    glColor3dv(colors[1]);
//    set_materials(&redPlasticMaterials);
//    glNormal3fv(cubeNomals[1]);
    polygon(2, 3, 7, 6);
    glColor3dv(colors[2]);
//    set_materials(&greenPlasticMaterials);
//    glNormal3fv(cubeNomals[2]);
    polygon(3, 0, 4, 7);
    glColor3dv(colors[3]);
//    set_materials(&bluePlasticMaterials);
//    glNormal3fv(cubeNomals[3]);
    polygon(1, 2, 6, 5);
    glColor3dv(colors[4]);
//    set_materials(&whiteShinyMaterials);
//    glNormal3fv(cubeNomals[4]);
    polygon(4, 5, 6, 7);
    glColor3dv(colors[5]);
//    set_materials(&magentaMaterials);
//    glNormal3fv(cubeNomals[5]);
    polygon(5, 4, 0, 1);
}

void myDisplay(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    gluLookAt(5.0, 5.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0, 0.0, dis_trans);
    glRotated(theta[0], 1.0, 0.0, 0.0);
    glRotated(theta[1], 0.0, 1.0, 0.0);
    glRotated(theta[2], 0.0, 0.0, 1.0);
    glScaled(scale_x, scale_y, scale_z);
    glShadeModel(GL_FLAT);
    cube();
    glutSwapBuffers();
}

void spinCube()
{
    theta[axis] += 2.0;
    if (theta[axis] > 360.0) theta[axis] -= 360.0;
    glutPostRedisplay();
}

void myReshape(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (w <= h)
    {
        glOrtho(-10.0, 10.0,
                -10.0  * (GLfloat)h / (GLfloat)w,
                 10.0  * (GLfloat)h / (GLfloat)w,
                 -100.0, 100.0);
    }
    else
    {
        glOrtho(-10.0  * (GLfloat)w / (GLfloat)h,
                10.0  * (GLfloat)w / (GLfloat)h,
                -10.0, 10.0, -100.0, 100.0);
    }
    glViewport(0, 0, w, h);
    global_w = w;
    global_h = h;
}

void myKeyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'q':
        {
            exit(0);
        }
        case 'z':
        {
            glLineWidth(5.0);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glutPostRedisplay();
            break;
        }
        case '1':
        case '2':
        case '3':
        {
            axis = (unsigned int)(key - 49);
            break;
        }
        case 't':
        {
            dis_trans -= 1;
            glutPostRedisplay();
            break;
        }
        case 'T':
        {
            dis_trans += 1;
            glutPostRedisplay();
            break;
        }
        case 'a':
        {
            scale_x += 2;
            glutPostRedisplay();
            break;
        }
        case 's':
        {
            scale_y += 2;
            glutPostRedisplay();
            break;
        }
        case 'd':
        {
            scale_z += 2;
            glutPostRedisplay();
            break;
        }
        default:
        {
            return;
        }
    }
}

void init(void)
{
//    glEnable(GL_LIGHT0);
//    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
//    set_lighting(&whiteLighting);
    glClearColor(0.0, 0.0, 0.0, 1.0);
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowPosition(200, 200);
    glutInitWindowSize(400, 400);
    glutCreateWindow("OpenGL Lab 1");
    glutReshapeFunc(myReshape);
    glutDisplayFunc(myDisplay);
    glutKeyboardFunc(myKeyboard);
    glutIdleFunc(spinCube);
    init();
    glutMainLoop();
    return 0;
}
