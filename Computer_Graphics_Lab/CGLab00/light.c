/*
 * This source file implements data and functions required for the light source setting.
 *
 * Created by Wen Chao
 *
 * 2016-04-19
 *
 * */

#include "light.h"

LightingStruct whiteLighting = {
        { 0.0f, 0.0f, 0.0f, 1.0f },
        { 1.0f, 1.0f, 1.0f, 1.0f },
        { 1.0f, 1.0f, 1.0f, 1.0f },
        { 10.0f, 10.0f, 10.0f, 1.0f },
        { 0.0f, 0.0f, 0.0f },
        180.0f,
};

LightingStruct coloredLighting = {
        { 0.2f, 0.0f, 0.0f, 1.0f },
        { 0.0f, 1.0f, 0.0f, 1.0f },
        { 0.0f, 0.0f, 1.0f, 1.0f },
        { 0.90f, 0.90f, 2.25f, 0.00f }
};

Normal cubeNomals[] = {
        { 0.0f, 0.0f,-1.0f },
        { 0.0f, 1.0f, 0.0f },
        {-1.0f, 0.0f, 0.0f },
        { 1.0f, 0.0f, 0.0f },
        { 0.0f, 0.0f, 1.0f },
        { 0.0f, 0.0f,-1.0f }
};
void set_lighting(LightingStruct *lightingPtr)
{
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightingPtr->ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightingPtr->diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightingPtr->specular);
    glLightfv(GL_LIGHT0, GL_POSITION, lightingPtr->position);
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, lightingPtr->spot_direction);
    glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, lightingPtr->spot_cutoff);
}
