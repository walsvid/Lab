/*
 * This head file contains data and functions required for the light source setting.
 *
 * Created by Wen Chao
 *
 * 2016-04-19
 *
 * */

#ifndef CGLAB01_LIGHT_H
#define CGLAB01_LIGHT_H

#include <glut/glut.h>

typedef struct LightingStruct
{
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat position[4];
    GLfloat spot_direction[3];
    GLfloat spot_cutoff;
}LightingStruct;

typedef GLfloat Normal[3];

extern LightingStruct whiteLighting;
extern LightingStruct coloredLighting;

extern Normal cubeNomals[];

void set_lighting(LightingStruct *lightingPtr);

#endif //CGLAB01_LIGHT_H
