/*
 * This head file contains data and functions required for the material setting.
 *
 * Created by Wen Chao
 *
 * 2016-04-19
 *
 * */

#ifndef CGLAB01_MATERIAL_H
#define CGLAB01_MATERIAL_H

#include <glut/glut.h>

typedef struct MaterialStruct
{
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat shininess;
}MaterialStruct;

extern MaterialStruct brassMaterials;
extern MaterialStruct redPlasticMaterials;
extern MaterialStruct greenPlasticMaterials;
extern MaterialStruct bluePlasticMaterials;
extern MaterialStruct whiteShinyMaterials;
extern MaterialStruct magentaMaterials;

void set_materials(MaterialStruct *materialPtr);

#endif //CGLAB01_MATERIAL_H
