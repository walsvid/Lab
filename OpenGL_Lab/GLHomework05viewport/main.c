#include <glut/glut.h>
#include <stdlib.h>

GLsizei global_w, global_h;

void polygon(int, int, int);
void tetrahedron1();
void tetrahedron2();
void tetrahedron3();

GLfloat vertex[][3] = { {0.0f, 0.0f, 0.0f}, {4.0f, 0.0f, 0.0f}, {0.0f, 4.0f, 0.0f}, {0.0f, 0.0f, 4.0f} };
GLfloat color[][3] = { {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f} };

void init(void)
{
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glClearColor(0.0, 0.0, 0.0, 1.0);
}
void myReshape(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (w <= h)
    {
        glOrtho(-10.0, 10.0,
                -10.0  * (GLfloat)h / (GLfloat)w,
                10.0  * (GLfloat)h / (GLfloat)w,
                -10.0, 100.0);
    }
    else
    {
        glOrtho(-10.0  * (GLfloat)w / (GLfloat)h,
                10.0  * (GLfloat)w / (GLfloat)h,
                -10.0, 10.0, -10.0, 100.0);
    }
    int W = w - 10;
    int H = h - 10;
    glViewport(0, 0, w, h);
//    glViewport(0, 0, w/2, h/2);
//    glViewport(w/2, 0, w/2, h/2);
//    glViewport(w/4, h/4, w/2, h/2);
//    glViewport(100, 50, 300, 300);
//    glViewport(w/2-W/2, h/2-H/2, W, H);
    global_w = w;
    global_h = h;
}
void myDisplay(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    gluLookAt(14.0 , 10.0, 4.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glShadeModel(GL_FLAT);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glLineWidth(3.0);
    tetrahedron1();
//    tetrahedron2();
//    tetrahedron3();
//    glutWireTeapot(4.0);
    glutSwapBuffers();
}

void myKeyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'q':
        {
            exit(0);
        }
        default:
        {
            return;
        }
    }
}
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowPosition(200, 200);
    glutInitWindowSize(400, 400);
    glutCreateWindow("Homework");
    init();
    glutReshapeFunc(myReshape);
    glutDisplayFunc(myDisplay);
    glutKeyboardFunc(myKeyboard);
    glutMainLoop();
    return 0;
}

void tetrahedron1()
{
    glColor3fv(color[0]);
    glBegin(GL_POLYGON);
    glVertex3fv(vertex[0]);
    glVertex3fv(vertex[1]);
    glVertex3fv(vertex[2]);
    glEnd();

    glColor3fv(color[1]);
    glBegin(GL_POLYGON);
    glVertex3fv(vertex[0]);
    glVertex3fv(vertex[2]);
    glVertex3fv(vertex[3]);
    glEnd();

    glColor3fv(color[2]);
    glBegin(GL_POLYGON);
    glVertex3fv(vertex[0]);
    glVertex3fv(vertex[3]);
    glVertex3fv(vertex[1]);
    glEnd();

    glColor3fv(color[3]);
    glBegin(GL_POLYGON);
    glVertex3fv(vertex[1]);
    glVertex3fv(vertex[2]);
    glVertex3fv(vertex[3]);
    glEnd();
}

void polygon(int a, int b, int c)
{
    glBegin(GL_POLYGON);
        glVertex3fv(vertex[a]);
        glVertex3fv(vertex[b]);
        glVertex3fv(vertex[c]);
    glEnd();
}

void tetrahedron2()
{
    glColor3f(1.0f, 0.0f, 0.0f);
    polygon(0, 1, 2);
    glColor3f(0.0f, 1.0f, 0.0f);
    polygon(0, 2, 3);
    glColor3f(0.0f, 0.0f, 1.0f);
    polygon(0, 3, 1);
    glColor3f(1.0f, 1.0f, 0.0f);
    polygon(1, 2, 3);
}
void tetrahedron3()
{
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    GLubyte tetrahedronIndices[] = {
                                    2, 3, 0,
                                    3, 0, 1,
                                    0, 1, 2,
                                    1, 2, 3,};
    glVertexPointer(3, GL_FLOAT, 0, vertex);
    glColorPointer(3, GL_FLOAT, 0, color);
    glDrawElements(GL_TRIANGLE_STRIP, 12, GL_UNSIGNED_BYTE, tetrahedronIndices);
}