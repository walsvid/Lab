//
// Created by wen on 16/4/20.
//

#ifndef GLHOMEWORK04ROBOT_ROBOT_H
#define GLHOMEWORK04ROBOT_ROBOT_H

#include <glut/glut.h>

typedef struct {
    GLfloat torso_height;
    GLfloat torso_base;
    GLfloat torso_top;
} Torso;

typedef struct {
    GLfloat head_x;
    GLfloat head_y;
    GLfloat head_r;
}Head;

typedef struct {
    GLfloat arm_x;
    GLfloat arm_y;
    GLfloat arm_w;
    GLfloat arm_h;
}Arm;

extern Torso torso;
extern Head head;
extern Arm left_upper_arm, left_lower_arm, right_upper_arm, right_lower_arm;
extern Arm left_upper_leg, left_lower_leg, right_upper_leg, right_lower_leg;
extern GLUquadricObj *ptr_torso;
extern GLUquadricObj *ptr_head;

void draw_torso();
void draw_head();
void draw_limb(Arm limb);

#endif //GLHOMEWORK04ROBOT_ROBOT_H
