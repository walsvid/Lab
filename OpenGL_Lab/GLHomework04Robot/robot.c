//
// Created by wen on 16/4/20.
//

#include "robot.h"
Torso torso = { 20.0f, 7.0f, 7.0f };
Head head = { 5.0f, 20.0f, 5.0f };
Arm left_upper_arm  = {-8.0f, 14.0f, 3.0f, 5.0f };
Arm left_lower_arm  = {-8.0f,  5.0f, 3.0f, 5.0f };
Arm right_upper_arm = { 8.0f, 14.0f, 3.0f, 5.0f };
Arm right_lower_arm = { 8.0f,  5.0f, 3.0f, 5.0f };
Arm left_upper_leg  = {-4.0f, -10.0f, 3.0f, 10.0f };
Arm left_lower_leg  = {-4.0f, -5.0f, 3.0f, 5.0f };
Arm right_upper_leg = { 4.0f, -10.0f, 3.0f, 10.0f };
Arm right_lower_leg = { 4.0f, -5.0f, 3.0f, 5.0f };
GLUquadricObj *ptr_torso;
GLUquadricObj *ptr_head;
void draw_torso()
{
    glPushMatrix();
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    gluCylinder(ptr_torso, torso.torso_base, torso.torso_top, torso.torso_height, 20, 20);
    glPopMatrix();
}
void draw_head()
{
    glPushMatrix();
    gluSphere(ptr_head, head.head_r, 20, 20);
    glPopMatrix();
}

void draw_limb(Arm limb)
{
    glPushMatrix();
    glTranslatef(0.0, 0.5f * limb.arm_h, 0.0f);
    glScalef(limb.arm_w, limb.arm_h, limb.arm_w);
    glutSolidCube(1.0);
    glPopMatrix();
}