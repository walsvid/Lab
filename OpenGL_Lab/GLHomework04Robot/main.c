#include <glut/glut.h>
#include <stdlib.h>
#include "robot.h"

GLsizei global_w, global_h;
GLfloat theta[11];
GLint axis = 100;

extern GLUquadricObj *ptr_torso;
extern GLUquadricObj *ptr_head;
// Initial functions
void init();

// Callback functions
void robotReshape(int w, int h);

void robotKeyboard(unsigned char key, int x, int y);

void robotDisplay();

void spin();

int main(int argc, char* argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("GLHomework04Robot");
    init();
    glutKeyboardFunc(robotKeyboard);
    glutDisplayFunc(robotDisplay);
    glutReshapeFunc(robotReshape);
//    glutIdleFunc(spin);
    glutMainLoop();
    return 0;
}

// Reshape window, make sure the graphics are not deformed.
void robotReshape(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (w <= h)
    {
        glOrtho(-50.0, 50.0,
                -50.0  * (GLfloat)h / (GLfloat)w,
                50.0  * (GLfloat)h / (GLfloat)w,
                -100.0, 100.0);
    }
    else
    {
        glOrtho(-50.0  * (GLfloat)w / (GLfloat)h,
                50.0  * (GLfloat)w / (GLfloat)h,
                -50.0, 50.0, -100.0, 100.0);
    }
    glViewport(0, 0, w, h);
    global_w = w;
    global_h = h;
}

void robotDisplay()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    gluLookAt(0.0, 0.0, 35.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    glMatrixMode(GL_MODELVIEW);
    glShadeModel(GL_FLAT);
    glLoadIdentity();
    glColor3f(1.0f, 0.0f, 0.0f);
    glRotatef(theta[0], 0.0f, 1.0f, 0.0f);
    draw_torso();

    glPushMatrix();
    glTranslatef(0.0f, head.head_x, 0.0f);
    glRotatef(theta[1], 1.0f, 0.0f, 0.0f);
    glRotatef(theta[2], 0.0f, 1.0f, 0.0f);
    glTranslatef(0.0f, head.head_y, 0.0);
    glColor3f(0.0f, 1.0f, 0.0f);
    draw_head();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(left_upper_arm.arm_x, left_upper_arm.arm_y, 0.0f);
    glRotatef(theta[3], 1.0f, 0.0f, 0.0f);
    glColor3f(1.0f, 1.0f, 0.0f);
    draw_limb(left_upper_arm);
    glTranslatef(0.0f, left_lower_arm.arm_y, 0.0f);
    glRotatef(theta[4], 1.0f, 0.0f, 0.0f);
    glColor3f(0.0f, 1.0f, 1.0f);
    draw_limb(left_lower_arm);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(right_upper_arm.arm_x, right_upper_arm.arm_y, 0.0f);
    glRotatef(theta[5], 1.0f, 0.0f, 0.0f);
    glColor3f(1.0f, 0.0f, 1.0f);
    draw_limb(right_upper_arm);
    glTranslatef(0.0f, right_lower_arm.arm_y, 0.0f);
    glRotatef(theta[6], 1.0f, 0.0f, 0.0f);
    glColor3f(0.0f, 0.0f, 1.0f);
    draw_limb(right_lower_arm);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(left_upper_leg.arm_x, left_upper_leg.arm_y, 0.0f);
    glRotatef(theta[7], 1.0f, 0.0f, 0.0f);
    glColor3f(1.0f, 0.0f, 1.0f);
    draw_limb(left_upper_leg);
    glTranslatef(0.0f, left_lower_leg.arm_y, 0.0f);
    glRotatef(theta[8], 1.0f, 0.0f, 0.0f);
    glColor3f(0.0f, 0.0f, 1.0f);
    draw_limb(left_lower_leg);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(right_upper_leg.arm_x, right_upper_leg.arm_y, 0.0f);
    glRotatef(theta[9], 1.0f, 0.0f, 0.0f);
    glColor3f(1.0f, 0.0f, 1.0f);
    draw_limb(right_upper_leg);
    glTranslatef(0.0f, right_lower_leg.arm_y, 0.0f);
    glRotatef(theta[10], 1.0f, 0.0f, 0.0f);
    glColor3f(0.0f, 0.0f, 1.0f);
    draw_limb(right_lower_leg);
    glPopMatrix();

    glutSwapBuffers();
}

void robotKeyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'q':
        {
            exit(0);
        }
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        {
            axis = (unsigned int)(key - 48);
            spin();
            break;
        }
        case '-':
        {
            axis = (unsigned int)(key - 35);
            spin();
            break;
        }
        default:
        {
            return;
        }
    }
}

void init()
{
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glClearColor(0.0, 0.0, 0.0, 1.0);
    ptr_torso = gluNewQuadric();
    ptr_head = gluNewQuadric();
}

// Functions to control model
void spin()
{
    theta[axis] += 2.0;
    if (theta[axis] > 360.0) theta[axis] -= 360.0;
    glutPostRedisplay();
}
