/*
 * This Project is a basic computer graphic lab.
 *
 * Aim: 0. Draw an object using functions in OpenGL, glut* is forbidden.
 *
 *      1. Implement a 3D object spin, translation and scale.
 *
 *      2. Setting materials and lighting.
 *
 *      3. Using the keyboard callback function.
 *
 * Created by Wen Chao
 *
 * 2016-05-06
 *
 * */

#include "utils.h"

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(200, 200);
    glutCreateWindow("GLHomework06 Spin Cube");

    glutReshapeFunc(myReshape);
    glutDisplayFunc(myDisplay);
    glutKeyboardFunc(myKeyboard);
    glutIdleFunc(spinCube);
    init();

    glutMainLoop();

    return 0;
}