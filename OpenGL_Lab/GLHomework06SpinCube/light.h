#ifndef GLHOMEWORK06SPINCUBE_LIGHT_H
#define GLHOMEWORK06SPINCUBE_LIGHT_H

#ifdef __APPLE__
#include <glut/glut.h>
#else
#include <GL/glut.h>
#endif

typedef struct LightingStruct
{
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat position[4];
    GLfloat spot_cutoff;
}LightingStruct;

typedef GLfloat Normal[3];

extern LightingStruct whiteLighting;
extern LightingStruct coloredLighting;

extern Normal cubeNomals[];

void set_lighting(LightingStruct *lightingPtr);

#endif //GLHOMEWORK06SPINCUBE_LIGHT_H
