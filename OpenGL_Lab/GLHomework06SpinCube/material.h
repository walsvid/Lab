/*
 * This head file contains data and functions required for the material setting.
 *
 * Created by Wen Chao
 *
 * 2016-05-06
 *
 * */

#ifndef GLHOMEWORK06SPINCUBE_MATERIAL_H
#define GLHOMEWORK06SPINCUBE_MATERIAL_H

#ifdef __APPLE__
#include <glut/glut.h>
#else
#include <GL/glut.h>
#endif

typedef struct MaterialStruct
{
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat shininess;
}MaterialStruct;

extern MaterialStruct brassMaterials;
extern MaterialStruct redPlasticMaterials;
extern MaterialStruct greenPlasticMaterials;
extern MaterialStruct bluePlasticMaterials;
extern MaterialStruct whiteShinyMaterials;
extern MaterialStruct magentaMaterials;

void set_materials(MaterialStruct *materialPtr);

#endif //GLHOMEWORK06SPINCUBE_MATERIAL_H
