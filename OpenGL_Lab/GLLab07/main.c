#ifdef __APPLE__
#include <glut/glut.h>
#else
#include <GL/glut.h>
#endif

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

GLdouble R_top = 3.0;
GLdouble R_bottom = 5.0;
GLdouble h = 8.0;

void init()
{
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glClearColor(0.0, 0.0, 0.0, 1.0);
}

void drawCoord()
{
    glBegin(GL_LINES);
        glColor3d(1, 0, 0);
        glVertex3d(0, 0, 0);
        glVertex3d(10, 0, 0);
        glColor3d(0, 1, 0);
        glVertex3d(0, 0, 0);
        glVertex3d(0, 10, 0);
        glColor3d(0, 0, 1);
        glVertex3d(0, 0, 0);
        glVertex3d(0, 0, 10);
    glEnd();
}


void drawCone()
{
    glColor3d(1, 1, 1);

    glBegin(GL_TRIANGLES);
    for (GLdouble i = 0; i < 2 * M_PI; i += 0.4)
    {
        glVertex3d(0, 0, 0);
        glVertex3d(R_bottom * cos(i), 0, R_bottom * sin(i));
        glVertex3d(R_bottom * cos((i + 0.2)), 0, R_bottom * sin(i + 0.2));
    }
    glEnd();

    glBegin(GL_TRIANGLES);
    for (GLdouble i = 0; i < 2 * M_PI; i += 0.4)
    {
        glVertex3d(0, h, 0);
        glVertex3d(R_top * cos(i), h, R_top * sin(i));
        glVertex3d(R_top * cos((i + 0.2)), h, R_top * sin(i + 0.2));
    }
    glEnd();

    glColor3d(0.9,0.9,0.9);
    glBegin(GL_TRIANGLES);
    for (GLdouble i = 0; i < 2 * M_PI; i += 0.2)
    {
        glVertex3d(R_top * cos(i), h, R_top * sin(i));
        glVertex3d(R_bottom * cos(i), 0, R_bottom * sin(i));
        glVertex3d(R_bottom * cos((i + 0.2)), 0, R_bottom * sin(i + 0.2));
    }
    glEnd();
}


void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    gluLookAt(5.0, 5.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    drawCoord();
    drawCone();
    glutSwapBuffers();
}

void keybpard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'w':
            R_top += 1.0;
            glutPostRedisplay();
            break;
        case 's':
            R_top -= 1.0;
            glutPostRedisplay();
            break;
        case 'e':
            R_bottom += 1.0;
            glutPostRedisplay();
            break;
        case 'd':
            R_bottom -= 1.0;
            glutPostRedisplay();
            break;
        case 'q':
            h += 1.0;
            glutPostRedisplay();
            break;
        case 'a':
            h -= 1.0;
            glutPostRedisplay();
            break;
        default: break;
    }
}

void reshape(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (w <= h)
    {
        glOrtho(-10.0, 10.0,
                -10.0  * (GLfloat)h / (GLfloat)w,
                10.0  * (GLfloat)h / (GLfloat)w,
                -100.0, 100.0);
    }
    else
    {
        glOrtho(-10.0  * (GLfloat)w / (GLfloat)h,
                10.0  * (GLfloat)w / (GLfloat)h,
                -10.0, 10.0, -100.0, 100.0);
    }
    glViewport(0, 0, w, h);
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowPosition(200, 200);
    glutInitWindowSize(400, 400);
    glutCreateWindow("OpenGL Lab 01-3");

    //Callback functions
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keybpard);

    init();
    glutMainLoop();
    return 0;
}