#include <glut/glut.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

GLfloat angleHour = 0.0f;
GLfloat angleMinute = 0.0f;
GLfloat angleSecond = 0.0f;
GLUquadricObj *ptrDial;
GLfloat pi_2 = (GLfloat) (2 * M_PI);

time_t current_time;
struct tm * tm_local;

void createHourHand()
{
    glPushMatrix();
    glColor3f(0.0f, 1.0f, 0.0f);
    glRotatef(angleHour, 0.0f, 0.0f, 1.0f);
    glTranslated(0.0f, 1.0f, 0.3f);
    glScalef(0.4f, 2.0f, 0.01f);
    glutSolidCube(1.0);
    glPopMatrix();
}

void createMinuteHand()
{
    glPushMatrix();
    glColor3f(0.0f, 0.0f, 1.0f);
    glRotatef(angleMinute, 0.0f, 0.0f, 1.0f);
    glTranslated(0.0f, 2.0f, 0.4f);
    glScalef(0.2f, 4.0f, 0.01f);
    glutSolidCube(1.0);
    glPopMatrix();
}

void createSecondHand()
{
    glPushMatrix();
    glColor3f(1.0f, 1.0f, 0.0f);
    glRotatef(angleSecond, 0.0f, 0.0f, 1.0f);
    glTranslated(0.0f, 3.0f, 0.5f);
    glScalef(0.1f, 6.0f, 0.01f);
    glutSolidCube(1.0);
    glPopMatrix();
}

void createDial()
{
    glPushMatrix();
    glColor3f(0.7f, 0.0f, 0.7f);
    gluDisk(ptrDial, 5.8f, 6.1f, 40, 20);
    glPopMatrix();
    glColor3f(0.5f, 0.5f, 0.8f);
    glBegin(GL_TRIANGLE_FAN);
    glVertex3f(0.0f, 0.0f, 0.0f);
    for (int i = 0; i <= 360; i+= 6) {
        glColor3f(0.6f, 0.6f, 0.6f);
        GLfloat cal_x = (GLfloat) ( 6 * sin(i * pi_2 / 360) );
        GLfloat cal_y = (GLfloat) ( 6 * cos(i * pi_2 / 360) );
        glVertex3f(cal_x, cal_y,0.0f);
    }
    glEnd();
}

void createScale1()
{
    glColor3f(1.0, 0.1, 0.0);
    for (GLint ang = 0; ang < 360; ang += 90) {
        glPushMatrix();
        glRotatef(ang, 0, 0, 1);
        glTranslated(0.0f, 5.6f, 0.1f);
        glScalef(0.3f, 1.2f, 0.01f);
        glutSolidCube(1.0);
        glPopMatrix();
    }
}

void createScale2()
{
    glColor3f(1.0f, 0.0f, 1.0f);
    for (GLint ang = 0; ang < 360; ang+=30) {
        if (ang % 90 != 0) {
            glPushMatrix();
            glRotatef(ang, 0, 0, 1);
            glTranslated(0.0f, 5.2f, 0.2f);
            glScalef(0.1f, 1.5f, 0.01f);
            glutSolidCube(1.0);
            glPopMatrix();
        }
    }

}

void createWatch()
{
    createHourHand();
    createMinuteHand();
    createSecondHand();
    createScale1();
    createScale2();
    createDial();
}

void processWatch()
{
    current_time = time(NULL);
    tm_local = localtime(&current_time);
    angleSecond = -6 * tm_local->tm_sec;
    angleMinute = -6 * tm_local->tm_min;
    angleHour = -30 * tm_local->tm_hour;
    glutPostRedisplay();
}


void myReshape(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (w <= h)
    {
        glOrtho(-10.0, 10.0,
                -10.0  * (GLfloat)h / (GLfloat)w,
                10.0  * (GLfloat)h / (GLfloat)w,
                -100.0, 100.0);
    }
    else
    {
        glOrtho(-10.0  * (GLfloat)w / (GLfloat)h,
                10.0  * (GLfloat)w / (GLfloat)h,
                -10.0, 10.0, -100.0, 100.0);
    }
    glViewport(0, 0, w, h);
}

void myDisplay()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);
    createWatch();
    glutSwapBuffers();
}

void init(void)
{
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    ptrDial =  gluNewQuadric();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowPosition(200, 200);
    glutInitWindowSize(400, 400);
    glutCreateWindow("OpenGL Lab 2 01");
    init();
    glutDisplayFunc(myDisplay);
    glutReshapeFunc(myReshape);
    glutIdleFunc(processWatch);
    glutMainLoop();
    return 0;
}
