#include <glut/glut.h>
#include <stdlib.h>
#include <stdio.h>

#define MAX_OBJ_NUM 1000
#define BUFFER_SIZE 512

typedef struct
{
    GLfloat red;
    GLfloat green;
    GLfloat blue;
} GraphColor;

typedef struct
{
    GLfloat x;
    GLfloat y;
} Coord;

typedef struct
{
    Coord Point[3];
} GraphPoints;

typedef struct
{
    GLint _type;
    GraphColor _color;
    GraphPoints _points;
    GLenum _mode;
} Graph;

Graph graphList[MAX_OBJ_NUM];
GraphPoints currentPoints;
GLint maxHit = 0;
GLint currentHit = 0;

GLint count = 0;
GLint highlightObjList[MAX_OBJ_NUM] = {0};
GLint currentGraphType = 0;
GLint highCount = 0;

GLenum fillModeEnum;
GLenum selectModeEnum;
GLfloat neww,newh;

GraphColor graphColor;

void menu(GLint value)
{
    //Choose graph type
    //Choose graph color
    //Optional graph fill mode
    if (value == 3)
    {
        //Choose graph highlight
        selectModeEnum = GL_SELECT;
    }
    else if (value == 4)
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glFlush();
    }
}

void drawGraph(GLenum mode)
{
    GLint i;
    for (i = 1; i <= count; i++)
    {
        if (mode == GL_SELECT)
        {
            glLoadName((GLuint)i);
        }
        glColor3f(graphList[i]._color.red, graphList[i]._color.green, graphList[i]._color.blue);
        glPolygonMode(GL_FRONT_AND_BACK, graphList[i]._mode);

        if (graphList[i]._type == 1)        //Line
        {
            glBegin(GL_LINES);
                glVertex2f(graphList[i]._points.Point[0].x, graphList[i]._points.Point[0].y);
                glVertex2f(graphList[i]._points.Point[1].x, graphList[i]._points.Point[1].y);
            glEnd();
        }
        else if (graphList[i]._type == 2)   //Rect
        {
            glRectf(graphList[i]._points.Point[0].x,
                    graphList[i]._points.Point[0].y,
                    graphList[i]._points.Point[1].x,
                    graphList[i]._points.Point[1].y);
        }
        else if (graphList[i]._type == 3)   //Tri
        {
            glBegin(GL_POLYGON);
                glVertex2f(graphList[i]._points.Point[0].x, graphList[i]._points.Point[0].y);
                glVertex2f(graphList[i]._points.Point[1].x, graphList[i]._points.Point[1].y);
                glVertex2f(graphList[i]._points.Point[2].x, graphList[i]._points.Point[2].y);
            glEnd();
        }
    }
}

void subMenuGraphType(GLint value)
{
    if (value == 1)
    {
        currentGraphType = 1;
        maxHit = 2;
        currentHit = 0;
    }
    else if (value == 2)
    {
        currentGraphType = 2;
        maxHit = 2;
        currentHit = 0;
    }
    else if (value == 3)
    {
        currentGraphType = 3;
        maxHit = 3;
        currentHit = 0;
    }
}

void subMenuColor(GLint value)
{
    if (value == 1) //Red
    {
        graphColor.red = 0.8;
        graphColor.green = 0.0;
        graphColor.blue = 0.0;
    }
    else if (value == 2) //Green
    {
        graphColor.red = 0.0;
        graphColor.green = 0.8;
        graphColor.blue = 0.0;
    }
    else if (value == 3) //Blue
    {
        graphColor.red = 0.0;
        graphColor.green = 0.0;
        graphColor.blue = 0.8;
    }
    else                //White
    {
        graphColor.red = 0.8;
        graphColor.green = 0.8;
        graphColor.blue = 0.8;
    }
}

void subMenuFillMode(GLint value)
{
    if (value == 1)
    {
        fillModeEnum = GL_FILL;
    }
    else if (value == 2)
    {
        fillModeEnum = GL_LINE;
    }
}

void highLightOn(int highlightNum)
{
    highCount++;
    highlightObjList[highCount] = highlightNum;
    if (graphList[highlightNum]._color.red == 0.8)
    {
        graphList[highlightNum]._color.red = 1.0;
    }
    else if (graphList[highlightNum]._color.green == 0.8)
    {
        graphList[highlightNum]._color.green = 1.0;
    }
    else if (graphList[highlightNum]._color.blue == 0.8)
    {
        graphList[highlightNum]._color.blue = 1.0;
    }
    else
    {
        graphList[highlightNum]._color.red = 1.0;
        graphList[highlightNum]._color.green = 1.0;
        graphList[highlightNum]._color.blue = 1.0;//highlight white
    }
}

void highLightOff()
{
    for (; highCount > 0; highCount--)
    {
        if (graphList[highlightObjList[highCount]]._color.red == 1.0)
        {
            graphList[highlightObjList[highCount]]._color.red = 0.8;
        }
        else if (graphList[highlightObjList[highCount]]._color.green == 1.0)
        {
            graphList[highlightObjList[highCount]]._color.green = 0.8;
        }
        else if (graphList[highlightObjList[highCount]]._color.blue == 1.0)
        {
            graphList[highlightObjList[highCount]]._color.blue = 0.8;
        }
        else
        {
            graphList[highlightObjList[highCount]]._color.red = 1.0;
            graphList[highlightObjList[highCount]]._color.green = 1.0;
            graphList[highlightObjList[highCount]]._color.blue = 1.0; //dim white
        }
    }
}

void processHits(GLint hits, GLuint buff[])
{
    printf("hit\n");
    unsigned int i, j;
    GLint names, *ptr;
    printf("hits = %d\n", hits);
    ptr = (GLint *) buff;
    if (hits == 0)
    {highLightOff();return;}
    for (i = 0; i < hits; i++)
    {
        names = *ptr;
        ptr+=3;
        for (j = 0; j < names; j++)
        {
            highLightOn(*ptr);
            ptr++;

        }
    }

}

void init()
{
    glClearColor (0.0, 0.0, 0.0, 0.0);

    GLint subMnuType, subMnuColor, subMnuFill;

    subMnuType = glutCreateMenu(subMenuGraphType);
    glutAddMenuEntry("Line", 1);
    glutAddMenuEntry("Rectangle", 2);
    glutAddMenuEntry("Triangle", 3);

    subMnuColor = glutCreateMenu(subMenuColor);
    glutAddMenuEntry("Red", 1);
    glutAddMenuEntry("Green", 2);
    glutAddMenuEntry("Blue", 3);
    glutAddMenuEntry("White", 4);

    subMnuFill = glutCreateMenu(subMenuFillMode);
    glutAddMenuEntry("Fill" ,1);
    glutAddMenuEntry("Outline", 2);

    glutCreateMenu(menu);
    glutAddSubMenu("Draw Graph", subMnuType);
    glutAddSubMenu("Choose Color", subMnuColor);
    glutAddSubMenu("Fill Mode", subMnuFill);
    glutAddMenuEntry("Select Mode", 3);
    glutAddMenuEntry("Clear Screen", 4);

    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    drawGraph(GL_RENDER);
    glFlush();
}

void reshape(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, w, 0, h);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    neww = w;
    newh = h;
}

void keyboard(unsigned char key, GLint x, GLint y)
{
    switch (key)
    {
        case 27: //ESC
            exit(0);
        default:
            break;
    }
}

void mouse(GLint button ,GLint state, GLint x, GLint y)
{
    GLuint selectBuff[BUFFER_SIZE];
    GLint hits;
    GLint viewpoint[4];
    glGetIntegerv(GL_VIEWPORT, viewpoint);

    if ( (selectModeEnum == GL_SELECT) && (button == GLUT_LEFT_BUTTON) && (state == GLUT_DOWN) )
    {
        highLightOff();
        glSelectBuffer(BUFFER_SIZE, selectBuff);
        glRenderMode(GL_SELECT);

        glInitNames();
        glPushName(0);

        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();

        gluPickMatrix((GLdouble) x, (GLdouble) (viewpoint[3] - y), 5.0, 5.0, viewpoint);
        gluOrtho2D(0, neww, 0, newh);

        drawGraph(GL_SELECT);

        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glFlush();

        hits = glRenderMode(GL_RENDER);
        processHits(hits, selectBuff);
    }
    else if ( (button == GLUT_LEFT_BUTTON) && (state == GLUT_DOWN) )
    {
        currentPoints.Point[currentHit].x = (GLfloat) x;
        currentPoints.Point[currentHit].y = (GLfloat) (viewpoint[3] - y);
        currentHit++;
        if (currentHit == maxHit)
        {
            count++;
            graphList[count]._color = graphColor;
            graphList[count]._points = currentPoints;
            printf("%f",graphList[count]._points.Point[0].x);
            graphList[count]._type = currentGraphType;
            graphList[count]._mode = fillModeEnum;
            currentHit = 0;
        }
    }
    glutPostRedisplay();
}

int main(GLint argc, char* argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("GLHomework03DrawingBoard");

    init();
    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutMouseFunc(mouse);
    glutKeyboardFunc(keyboard);

    glutMainLoop();
    return 0;
}
