#ifndef GLHOMEWORK07TEX_COMMON_H
#define GLHOMEWORK07TEX_COMMON_H
#ifdef __APPLE__
#include <glut/glut.h>
#else
#include <GL/glut.h>
#endif
#include <stdlib.h>
#include <stdlib.h>

#include "light.h"
#include "material.h"

/*******************************Data declaration**********************************************/

extern GLsizei global_w, global_h;     // Set global window size
extern GLint dis_trans;
extern GLdouble scale_x;
extern GLdouble scale_y;
extern GLdouble scale_z;
extern GLint axis;
extern GLfloat theta[3];
extern GLdouble cubeIndices[][3];
extern GLdouble colorIndices[][3];
extern GLdouble normalIndices[][3];
extern MaterialStruct *currentMaterials;

/*******************************Function declaration**********************************************/

void polygon(GLint a, GLint b, GLint c, GLint d);

void setCube();

void spinCube();

void myDisplay(void);

void myReshape(int w, int h);

void myKeyboard(unsigned char key, int x, int y);

void init(void);

/*******************************Data declaration**********************************************/

extern GLsizei global_w, global_h;     // Set global window size
extern GLint dis_trans;
extern GLdouble scale_x;
extern GLdouble scale_y;
extern GLdouble scale_z;
extern GLint axis;
extern GLfloat theta[3];
extern GLdouble cubeIndices[][3];
extern GLdouble colorIndices[][3];
extern GLdouble normalIndices[][3];
extern MaterialStruct *currentMaterials;

/*******************************Function declaration**********************************************/

void polygon(GLint a, GLint b, GLint c, GLint d);

void setCube();

void spinCube();

void myDisplay(void);

void myReshape(int w, int h);

void myKeyboard(unsigned char key, int x, int y);

void init(void);
#endif //GLHOMEWORK07TEX_COMMON_H
