#include "common.h"


GLsizei global_w, global_h;     // Set global window size
GLint dis_trans = 0;
GLdouble scale_x= 1.0;
GLdouble scale_y = 1.0;
GLdouble scale_z = 1.0;
GLint axis = 10;
GLfloat theta[3];
GLdouble cubeIndices[][3] = { { -1.0,-1.0, 1.0 }, { -1.0, 1.0, 1.0 },
                              {  1.0, 1.0, 1.0 }, {  1.0,-1.0, 1.0 },
                              { -1.0,-1.0,-1.0 }, { -1.0, 1.0,-1.0 },
                              {  1.0, 1.0,-1.0 }, {  1.0,-1.0,-1.0 } };

GLdouble colorIndices[][3] = { { 1.0, 0.0, 0.0 }, { 0.0, 1.0, 1.0 },
                               { 1.0, 1.0, 0.0 }, { 0.0, 1.0, 0.0 },
                               { 0.0, 0.0, 1.0 }, { 1.0, 0.0, 1.0} };

GLdouble normalIndices[][3] = { { 0.0, 0.0, 1.0 }, { 1.0, 0.0, 0.0 },
                                { 0.0,-1.0, 0.0 }, { 0.0, 1.0, 0.0 },
                                { 0.0, 0.0,-1.0 }, {-1.0, 0.0, 0.0 } };

MaterialStruct *currentMaterials = &greenPlasticMaterials;

void polygon(GLint a, GLint b, GLint c, GLint d)
{
    glBegin(GL_POLYGON);
    glVertex3dv(cubeIndices[a]);
    glVertex3dv(cubeIndices[b]);
    glVertex3dv(cubeIndices[c]);
    glVertex3dv(cubeIndices[d]);
    glEnd();
}

void setCube()
{
    glColor3dv(colorIndices[0]);
    set_materials(currentMaterials);
    glNormal3dv(normalIndices[0]);
    polygon(0, 3, 2, 1);
    glColor3dv(colorIndices[1]);
    glNormal3dv(normalIndices[1]);
    polygon(2, 3, 7, 6);
    glColor3dv(colorIndices[2]);
    glNormal3dv(normalIndices[2]);
    polygon(3, 0, 4, 7);
    glColor3dv(colorIndices[3]);
    glNormal3dv(normalIndices[3]);
    polygon(1, 2, 6, 5);
    glColor3dv(colorIndices[4]);
    glNormal3dv(normalIndices[4]);
    polygon(4, 5, 6, 7);
    glColor3dv(colorIndices[5]);
    glNormal3dv(normalIndices[5]);
    polygon(5, 4, 0, 1);
}

void myDisplay(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    gluLookAt(5.0, 5.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    glTranslatef(0.0, 0.0, dis_trans);
    glRotated(theta[0], 1.0, 0.0, 0.0);
    glRotated(theta[1], 0.0, 1.0, 0.0);
    glRotated(theta[2], 0.0, 0.0, 1.0);
    glScaled(scale_x, scale_y, scale_z);
    glShadeModel(GL_FLAT);
    setCube();
    glPopMatrix();
    glutSwapBuffers();
}

void spinCube()
{
    theta[axis] += 2.0;
    if (theta[axis] > 360.0) theta[axis] -= 360.0;
    glutPostRedisplay();
}

void myReshape(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (w <= h)
    {
        glOrtho(-10.0, 10.0,
                -10.0  * (GLfloat)h / (GLfloat)w,
                10.0  * (GLfloat)h / (GLfloat)w,
                -100.0, 100.0);
    }
    else
    {
        glOrtho(-10.0  * (GLfloat)w / (GLfloat)h,
                10.0  * (GLfloat)w / (GLfloat)h,
                -10.0, 10.0, -100.0, 100.0);
    }
    glViewport(0, 0, w, h);
    global_w = w;
    global_h = h;
}

void myKeyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 27:
        {
            exit(0);
        }
        case 'z':
        {
            glLineWidth(2.0);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glutPostRedisplay();
            break;
        }
        case 'Z':
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glutPostRedisplay();
            break;
        }
        case '1':
        case '2':
        case '3':
        {
            axis = (unsigned int)(key - 49);
            break;
        }
        case '4':
        {
            currentMaterials = &whiteShinyMaterials;
            glutPostRedisplay();
            break;
        }
        case '5':
        {
            currentMaterials = &brassMaterials;
            glutPostRedisplay();
            break;
        }
        case '6':
        {
            currentMaterials = &redPlasticMaterials;
            glutPostRedisplay();
            break;
        }
        case '7':
        {
            currentMaterials = &bluePlasticMaterials;
            glutPostRedisplay();
            break;
        }
        case '8':
        {
            currentMaterials = &greenPlasticMaterials;
            glutPostRedisplay();
            break;
        }
        case 't':
        {
            dis_trans -= 1;
            glutPostRedisplay();
            break;
        }
        case 'T':
        {
            dis_trans += 1;
            glutPostRedisplay();
            break;
        }
        case 'a':
        {
            scale_x += 2;
            glutPostRedisplay();
            break;
        }
        case 's':
        {
            scale_y += 2;
            glutPostRedisplay();
            break;
        }
        case 'd':
        {
            scale_z += 2;
            glutPostRedisplay();
            break;
        }
        case 'A':
        {
            scale_x -= 2;
            glutPostRedisplay();
            break;
        }
        case 'S':
        {
            scale_y -= 2;
            glutPostRedisplay();
            break;
        }
        case 'D':
        {
            scale_z -= 2;
            glutPostRedisplay();
            break;
        }
        default:
        {
            return;
        }
    }
}

void init(void)
{
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    set_lighting(&whiteLighting);
    glClearColor(0.0, 0.0, 0.0, 1.0);
}