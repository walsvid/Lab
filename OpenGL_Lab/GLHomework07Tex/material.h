//
// Created by wen on 16/5/15.
//

#ifndef GLHOMEWORK07TEX_MATERIAL_H
#define GLHOMEWORK07TEX_MATERIAL_H

#ifdef __APPLE__
#include <glut/glut.h>
#else
#include <GL/glut.h>
#endif

typedef struct MaterialStruct
{
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat shininess;
}MaterialStruct;

extern MaterialStruct brassMaterials;
extern MaterialStruct redPlasticMaterials;
extern MaterialStruct greenPlasticMaterials;
extern MaterialStruct bluePlasticMaterials;
extern MaterialStruct whiteShinyMaterials;
extern MaterialStruct magentaMaterials;

void set_materials(MaterialStruct *materialPtr);

#endif //GLHOMEWORK07TEX_MATERIAL_H
