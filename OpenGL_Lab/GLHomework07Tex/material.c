#include "material.h"

MaterialStruct brassMaterials = {
        { 0.33f, 0.22f, 0.03f, 1.0f },
        { 0.78f, 0.57f, 0.11f, 1.0f },
        { 0.99f, 0.91f, 0.81f, 1.0f },
        27.8
};

MaterialStruct redPlasticMaterials = {
        { 0.3f, 0.0f, 0.0f },
        { 0.6f, 0.0f, 0.0f },
        { 0.8f, 0.6f, 0.6f },
        32.0
};

MaterialStruct greenPlasticMaterials = {
        { 0.0f, 0.3f, 0.0f },
        { 0.0f, 0.6f, 0.0f },
        { 0.6f, 0.8f, 0.6f },
        32.0
};

MaterialStruct bluePlasticMaterials = {
        { 0.0f, 0.0f, 0.3f },
        { 0.0f, 0.0f, 0.6f },
        { 0.6f, 0.6f, 0.8f },
        32.0
};

MaterialStruct whiteShinyMaterials = {
        { 1.0f, 1.0f, 1.0f },
        { 1.0f, 1.0f, 1.0f },
        { 1.0f, 1.0f, 1.0f },
        100.0
};

MaterialStruct magentaMaterials = {
        { 0.5f, 0.0f, 0.5f },
        { 0.3f, 0.4f, 0.1f },
        { 0.1f, 0.0f, 0.0f },
        24.5
};

void set_materials(MaterialStruct *materialPtr)
{
    // Define material properties for polygon
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, materialPtr->ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, materialPtr->diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, materialPtr->specular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, materialPtr->shininess);
}
