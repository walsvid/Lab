#include <glut/glut.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define DEEPMAX 10
static GLdouble ax=-1.0,ay=-1.0,bx=1.0,by=-1.0,cx=0.0,cy=1.0;

GLdouble calRandPosi(GLdouble oldPt, GLdouble globalPt, bool flag)
{
    GLdouble newPt = oldPt - (oldPt - globalPt)/10 + (0.2 * rand()/RAND_MAX-0.1);
    if (flag)
        newPt = (newPt > globalPt) ? newPt : globalPt;
    else
        newPt = (newPt < globalPt) ? newPt : globalPt;
    return newPt;
}

void divideShape(GLdouble x, GLdouble y, int deep)
{
    GLdouble lx, ly, rx, ry;
    if (deep == DEEPMAX) return;

    glBegin(GL_TRIANGLES);
      glVertex2d(ax,ay);
      glVertex2d(bx,by);
      glVertex2d(x,y);
    glEnd();

    //TODO:calculate random position
    lx = calRandPosi(x, ax, true);
    ly = calRandPosi(y, ay, true);
    rx = calRandPosi(x, bx, false);
    ry = calRandPosi(y, by, true);

    divideShape(lx, ly, deep + 1);
    divideShape(rx, ry, deep + 1);
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    srand((unsigned)(time(NULL)));
    divideShape(cx, cy, 1);
    glFlush();
}

void myReshape(GLsizei w, GLsizei h)
{
    glViewport(0,0,w,h);
}

void init()
{
    glClearColor(1.0, 1.0, 1.0, 0.0);
    glColor3f(0.0, 0.0, 0.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-2.0, -2.0, 2.0, 2.0);
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
    GLsizei w = 500, h = 500;
    glutInitWindowSize(w,h);
    glutInitWindowPosition(50,50);
    glutCreateWindow("Draw Mountain");
    glutDisplayFunc(display);
    init();
    glutMainLoop();
    return 0;
}
