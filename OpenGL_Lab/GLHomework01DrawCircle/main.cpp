#include <glut/glut.h>
#include <cmath>
#include <stdlib.h>

static GLint _n = 5;


void display()
{
    glClear(GL_COLOR_BUFFER_BIT);

    glBegin(GL_POLYGON);
    GLint n = (_n > 3) ? _n : 3;
    GLdouble theta = 2 * M_PI / n;
    for (int i = 0; i  < n; i++)
    {
        GLdouble pOnCircleX = (GLfloat)cos(i*theta);
        GLdouble pOnCircleY = (GLfloat)sin(i*theta);
        glVertex2d(pOnCircleX, pOnCircleY);
    }
    glEnd();

    glFlush();
}

void myReshape(GLsizei w, GLsizei h)
{
    glViewport(0,0,w,h);
}

static void changePointsNum(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'q':
            exit(0);
        case '-':
            --_n;
            break;
        case '+':
            ++_n;
            break;
        default:
            break;
    }

    glutPostRedisplay();
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    GLsizei w = 500;
    GLsizei h = 500;
    glutInitWindowSize(w, h);
    glutCreateWindow("Draw Circle");
    glutDisplayFunc(display);
    glutReshapeFunc(myReshape);
    glutKeyboardFunc(changePointsNum);
    glutMainLoop();
    return 0;
}
