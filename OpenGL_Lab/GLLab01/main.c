#include <glut/glut.h>
#include <math.h>

static GLdouble x[10], y[10], z[10];
static GLdouble a = 2.0,b = 3.0, c = 7.0;
GLfloat lineWidth = 2.0;

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0);

    glPointSize(1.0);
    glColor3f(1.0, 1.0, 0.0);
    glEnable(GL_LINE_STIPPLE);
    glLineWidth(lineWidth);

    glBegin(GL_LINE_STRIP);
    for (GLdouble i = 0; i < 2*M_PI; i+=0.02)
    {
        glVertex3d(a*i*cos(c*i)+b,a*i*sin(c*i)+b,c*i);
//        glVertex3d( (a*sin(c*i)+b)*cos(i), (a*sin(c*i)+b)*sin(i), a*cos(c*i));
    }
    glEnd();
    glColor3d(1, 1, 1);
    glLineWidth(1.0);
    glDisable(GL_LINE_STIPPLE);
    glBegin(GL_LINES);
        glVertex3d(0, 0, 0);
        glVertex3d(10, 0, 0);
        glVertex3d(0, 0, 0);
        glVertex3d(0, 10, 0);
        glVertex3d(0, 0, 0);
        glVertex3d(0, 0, 10);
    glEnd();
    glEnable(GL_LINE_STIPPLE);

    glutSwapBuffers();
}

void myReshape(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-20.0,20.0,-20.0,20.0,-100.0,100.0);
}

void mykeyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'w':
        {
            lineWidth += 1.0;
            glutPostRedisplay();
            break;
        }
        case 's':
        {
            if (lineWidth > 0)
            {
                lineWidth -= 1.0;
            }
            glutPostRedisplay();
            break;
        }
        case '1':
        {
            glLineStipple(1, 0xaaaa);
            glutPostRedisplay();
            break;
        }
        case '2':
        {
            glLineStipple(1, 0x1c47);
            glutPostRedisplay();
            break;
        }
        case '3':
        {
            glLineStipple(1, 0xffff);
            glutPostRedisplay();
            break;
        }
        case '4':
        {
            glLineStipple(1, 0xcccc);
            glutPostRedisplay();
            break;
        }
        default: break;
    }
}

void init()
{
    glClearColor(1.0,1.0,1.0,1.0);
    glColor3f(0.0,0.0,0.0);
}
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE| GLUT_RGB);
    glutInitWindowSize(500,500);
    glutInitWindowPosition(50,50);
    glutCreateWindow("Lab01");
    glutReshapeFunc(myReshape);
    glutDisplayFunc(display);
    glutKeyboardFunc(mykeyboard);
    glutMainLoop();
    init();
    return 0;
}