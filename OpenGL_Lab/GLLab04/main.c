/*
 *  OpenGL Lab 04
 *
 *  Author : Wen Chao
 *
 */

//头文件, 利用宏定义保证在多个平台上都可以正确编译
#ifdef __APPLE__
#include <glut/glut.h>
#else
#include <GL/glut.h>
#endif

//定义法向量
GLdouble normalIndices[][3] = { { 0.0, 0.0, 1.0 }, { 1.0, 0.0, 0.0 },
                                { 0.0,-1.0, 0.0 }, { 0.0, 1.0, 0.0 },
                                { 0.0, 0.0,-1.0 }, {-1.0, 0.0, 0.0 } };

//定义纹理的基本信息
#define checkImageWidth 128
#define checkImageHeight 128
GLubyte checkImage[checkImageHeight][checkImageWidth][4];
GLuint texName;

//定义颜色
GLdouble drakgreen[3] = {0, 0.4 ,0};
GLdouble drakBlue1[3] = {0, 0, 0.3};
GLdouble drakBlue2[3] = {0, 0, 0.5};
GLdouble drakred[4] = {0.3, 0, 0, 0.8};

//定义鼠标回调函数的全局记录坐标
GLfloat xx=0, yy=0;

//定义灯光类型
typedef struct light{
    GLfloat position[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat direction[3];
    GLint angle;
}light;

//定义材质类型
typedef struct material{
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat shininess;
}material;

light spotlight = {
        {0,0,0,1},
        {1,1,1,1},
        {1,1,1,1},
        {0, -1, 0},
        180
};

material celling = {
        {0, 0.3, 0, 1.0},
        {0, 0.6, 0, 1.0},
        {0.6, 0.8, 0.6, 1.0},
        32.0
};

material wall = {
        {0.2,0.2,0.4,1},
        {0.8,0.8,0.9,1},
        {1,1,1,1},
        32.0
};

material ground = {
        {0.3, 0, 0, 1.0},
        {0.6, 0, 0, 1.0},
        {0.8, 0.6, 0.6, 1.0},
        32.0
};

material furnish = {
        {0.33, 0.22, 0.03, 1.0},
        {0.78, 0.57, 0.11, 1.0},
        {0.99, 0.91, 0.81, 1.0},
        27.8
};

material sphere_m = {
        {0.33, 0.02, 0.43, 1.0},
        {0.78, 0.57, 0.11, 1.0},
        {0.99, 0.91, 0.81, 1.0},
        27.8
};

material shine = {
        {0.5,0.5,1,1},
        {0.4,0.5,1,1},
        {1,1,1,1},
        100
};

//定义材质指针
material *ptr_material = &sphere_m;

//光源位置
GLfloat pos_x = 0;
GLfloat pos_y = 0;
GLfloat pos_z = 0;

//实现棋盘格的函数
void makeCheckImage(void)
{
    int i, j, c;
    for (i = 0; i < checkImageHeight; i++)
    {
        for (j = 0; j < checkImageWidth; j++)
        {
            c = ((((i & 0x8) == 0) ^ ((j & 0x8)) == 0)) * 255;
            checkImage[i][j][0] = (GLubyte)c;
            checkImage[i][j][1] = (GLubyte)c;
            checkImage[i][j][2] = (GLubyte)c;
            checkImage[i][j][3] = (GLubyte)255;
        }
    }
}

//设置材质的函数
void set_materials(material *materialPtr)
{
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, materialPtr->ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, materialPtr->diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, materialPtr->specular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, materialPtr->shininess);
}

//设置光照的函数
void set_light(light* lightptr, GLenum lightNum)
{
    glLightfv(lightNum, GL_DIFFUSE, lightptr->diffuse);
    glLightfv(lightNum, GL_SPECULAR, lightptr->specular);
    glLightfv(lightNum, GL_POSITION, lightptr->position);
    glLightf(lightNum, GL_SPOT_CUTOFF, lightptr->angle);
    glLightfv(lightNum, GL_SPOT_DIRECTION, lightptr->direction);
    glLightf(lightNum, GL_SPOT_EXPONENT, 1);
}

//菜单函数,实现:
//          1. 光照开启关闭
//          2. 实现材质的 GL_REPLACE GL_MODULATE 切换
void menu(GLint value)
{
    switch (value)
    {
        case 1:
        {
            glDisable(GL_LIGHT0);
            glDisable(GL_LIGHTING);
            break;
        }
        case 2:
        {
            glEnable(GL_LIGHTING);
            glEnable(GL_LIGHT0);
            break;
        }
        case 3:
        {
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
            break;
        }
        case 4:
        {
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
            break;
        }
        default:
            break;
    }
    glutPostRedisplay();
}

//初始化函数
void init(void)
{
    //纹理相关
    makeCheckImage();
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glGenTextures(1, &texName);
    glBindTexture(GL_TEXTURE_2D, texName);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, checkImageWidth, checkImageHeight, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, checkImage);
    //光照相关
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    //调整投影矩阵
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90,1,1,100);
    //调整模型视图矩阵
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glClearColor(0.0, 0.0, 0.0, 1.0);
}

//绘制天花板
void createCelling()
{
    glColor3dv(drakgreen);
    set_materials(&celling);
    glNormal3dv(normalIndices[2]);
    for (GLdouble i = -10; i < 10; i += 0.1)
    {
        for (GLdouble j = -10; j < 10; j += 0.1)
        {
            glBegin(GL_POLYGON);
            glVertex3d(i    , 10, j    );
            glVertex3d(i    , 10, j + 1);
            glVertex3d(i + 1, 10, j + 1);
            glVertex3d(i + 1, 10, j    );
            glEnd();
        }
    }
}

//绘制墙壁
void createWall()
{
    set_materials(&wall);
    glNormal3dv(normalIndices[1]);
    glColor3dv(drakBlue1);
    for (GLdouble i = -10; i < 10; i += 0.1)
    {
        for (GLdouble j = -10; j < 10; j += 0.1)
        {
            glBegin(GL_POLYGON);
            glVertex3d(-10, i    , j    );
            glVertex3d(-10, i    , j + 1);
            glVertex3d(-10, i + 1, j + 1);
            glVertex3d(-10, i + 1, j    );
            glEnd();
        }
    }

    glColor3dv(drakBlue2);
    glNormal3dv(normalIndices[0]);
    for (GLdouble i = -10; i < 10; i += 0.1)
    {
        for (GLdouble j = -10; j < 10; j += 0.1)
        {
            glBegin(GL_POLYGON);
            glVertex3d(i    , j    , -10);
            glVertex3d(i    , j + 1, -10);
            glVertex3d(i + 1, j + 1, -10);
            glVertex3d(i + 1, j    , -10);
            glEnd();
        }
    }

    glColor3dv(drakBlue1);
    glNormal3dv(normalIndices[5]);
    for (GLdouble i = -10; i < 10; i += 0.1)
    {
        for (GLdouble j = -10; j < 10; j += 0.1)
        {
            glBegin(GL_POLYGON);
            glVertex3d(10, i    , j    );
            glVertex3d(10, i    , j + 1);
            glVertex3d(10, i + 1, j + 1);
            glVertex3d(10, i + 1, j    );
            glEnd();
        }
    }

}

//绘制带纹理的地板
void createGround()
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texName);

    set_materials(&ground);
    glColor4dv(drakred);
    glNormal3dv(normalIndices[3]);

    for (GLdouble i = -10; i < 10; i += 1)
    {
        for (GLdouble j = -10; j < 10; j+= 1)
        {
            glBegin(GL_POLYGON);
            glTexCoord2d((i + 10) / 20, (j + 10) / 20);
            glVertex3d(i, -10, j);
            glTexCoord2d((i + 10) / 20, (j + 11) / 20);
            glVertex3d(i, -10, j + 1);
            glTexCoord2d((i + 11) / 20, (j + 11) / 20);
            glVertex3d(i + 1, -10, j + 1);
            glTexCoord2d((i + 11) / 20, (j + 10) / 20);
            glVertex3d(i + 1, -10, j);
            glEnd();
        }
    }

    glDisable(GL_TEXTURE_2D);
}

//绘制内饰
void createFurnishings()
{
    set_materials(ptr_material);
    glColor3d(1, 1, 0);
    glPushMatrix();
    glTranslated(0, -8, -8);
    glutSolidSphere(2, 20, 20);
    glPopMatrix();

    set_materials(&furnish);
    glColor3d(1, 1, 1);
    glPushMatrix();
    glTranslated(-4, -10, -3);
    glRotated(-90, 1, 0, 0);
    glutSolidCone(2.0, 4.0 ,40, 10);
    glPopMatrix();
}

void createLightProxy()
{
//    GLfloat light_pos[] = {pos_x, pos_y, pos_z, 1};
//    GLfloat center[] = {0,-1,0};
    glPushMatrix();
    set_materials(&shine);
    glTranslated(pos_x, pos_y, pos_z);
    glPushAttrib(GL_LIGHTING_BIT);
    glDisable(GL_LIGHTING);
    glutSolidSphere(0.2, 20, 20);
    glPopAttrib();
    set_light(&spotlight, GL_LIGHT0);
    glPopMatrix();

}

//绘制整个屋子
void createWorld()
{
    glPushMatrix();
    glColor3d(1,1,1);
    createLightProxy();
    glPopMatrix();
    createCelling();
    createWall();
    createFurnishings();
}

//display回调函数
void render_room1()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0.0, 0.0, 10.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    GLfloat mirror[4] = {pos_x, -pos_y, pos_z, 1};
    GLfloat origin[4] = {pos_x, pos_y, pos_z, 1};

    //绘制带反光效果的地板
    glPushMatrix();

        glLightfv(GL_LIGHT0, GL_POSITION, mirror);
        glPushMatrix();
            glFrontFace(GL_CW);
            glTranslated(0, -10, 0);
            glScalef(1.0f, -1.0f, 1.0f);
            glTranslated(0, 10, 0);
            createWorld();
            glFrontFace(GL_CCW);
        glPopMatrix();
        glPushAttrib(GL_LIGHTING_BIT);
            glDisable(GL_LIGHTING);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            createGround();
            glDisable(GL_BLEND);
        glPopAttrib();

        glLightfv(GL_LIGHT0, GL_POSITION, origin);
        createWorld();

    glPopMatrix();

    glutSwapBuffers();
}

//键盘回调函数
void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case '0':
        {
            ptr_material = &sphere_m;
            glutPostRedisplay();
            break;
        }
        case '1':
        {
            ptr_material = &shine;
            glutPostRedisplay();
            break;
        }
        case 'w':
        {
            pos_z -= 1;
            glutPostRedisplay();
            break;
        }
        case 's':
        {
            pos_z += 1;
            glutPostRedisplay();
            break;
        }
        case 'a':
        {
            pos_x -= 1;
            glutPostRedisplay();
            break;
        }
        case 'd':
        {
            pos_x += 1;
            glutPostRedisplay();
            break;
        }
        case 'q':
        {
            pos_y += 1;
            glutPostRedisplay();
            break;
        }
        case 'e':
        {
            pos_y -= 1;
            glutPostRedisplay();
            break;
        }
        default:
        {
            ptr_material = &sphere_m;
        }
    }
}

//鼠标移动回调函数
void motion(int x,int y)
{
    GLfloat i=0,j=0;
    if(xx==0)xx=x;
    else
    {
        i=x-xx;
        xx=x;
    }
    if(yy==0)yy=y;
    else
    {
        j=y-yy;
        yy=y;
    }
    pos_x+=i/50.0;
    pos_y-=j/50.0;
    glutPostRedisplay();
}



int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(500, 500);
    glutCreateWindow("GLLab04");
    // Callback function
    glutDisplayFunc(render_room1);
    glutKeyboardFunc(keyboard);
    glutMotionFunc(motion);

    // Init function
    init();

    glutCreateMenu(menu);
    glutAddMenuEntry("Disable Lighting", 1);
    glutAddMenuEntry("Enable Lighting", 2);
    glutAddMenuEntry("GL_REPLACE", 3);
    glutAddMenuEntry("GL_MODULATE", 4);
    glutAttachMenu(GLUT_RIGHT_BUTTON);

    // Main loop
    glutMainLoop();
    return 0;
}