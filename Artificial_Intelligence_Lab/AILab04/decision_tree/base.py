import numpy as np
import math
import collections
import logging
import networkx as nx

logger = logging.getLogger('decision tree')
logger.setLevel(logging.INFO)


class DecisionTree(object):
    class DecisionTreeClassifyNode(object):
        def __init__(self, index, subtrees):
            self.index = index
            self.subtrees = subtrees  # type: dict

        def predict(self, test_data):
            test_value = test_data[self.index]
            min_dist = np.inf
            min_value = test_value
            for subtree_range in self.subtrees.keys():
                dist = abs(test_value - subtree_range)
                if dist < min_dist:
                    min_dist = dist
                    min_value = subtree_range
            sub_node = self.subtrees[min_value]
            return sub_node.predict(test_data)

        def __eq__(self, other):
            return isinstance(other, DecisionTree.DecisionTreeClassifyNode) \
                   and self.index == other.index \
                   and self.subtrees == other.subtrees

        def draw(self, graph, father_node):
            """
            visualize classify node of decision tree.
            :type father_node: str
            :type graph: nx.DiGraph
            """
            node_label = "Classify-Index-" + str(self.index) + str(id(self))
            graph.add_node(node_label)
            graph.add_edge(father_node, node_label)
            for sub_tree_node in self.subtrees.values():
                sub_tree_node.draw(graph, node_label)

        def export_to_json(self, json_tree_list):
            """
            Export classify node to json
            :type json_tree_list: list
            """
            label = "index" + str(self.index)
            current_dict = {"name": label, "children": []}
            for sub_value in self.subtrees:
                self.subtrees[sub_value].export_to_json(current_dict["children"])
            json_tree_list.append(current_dict)

    class DecisionTreeLeafNode(object):
        def __init__(self, label, data):
            self.label = label
            self.data = data
            logger.debug('LeafNode:' + 'label:' + str(label))

        def predict(self, test_data):
            return self.label

        def __eq__(self, other):
            return isinstance(other, DecisionTree.DecisionTreeLeafNode) \
                   and self.label == other.label

        def draw(self, graph, father_node):
            """
            visualize leaf node of decision tree.
            :type father_node: str
            :type graph: nx.DiGraph
            """
            node_label = "Leaf-Label-" + str(self.data)
            graph.add_node(node_label)
            graph.add_edge(father_node, node_label)

        def export_to_json(self, json_tree_list):
            """
            Export leaf node to json
            :type json_tree_list: list
            """
            label = "label" + str(self.label)
            for k, data in enumerate(self.data):
                data_string = "-".join(str(data_item) for data_item in data)
                current_dict = {"name": data_string, "label": label}
                json_tree_list.append(current_dict)

    def __init__(self):
        self.node = None

    def fit(self, x, y):
        """
        :type x: np.ndarray
        :param x:
        :type y: np.ndarray
        :param y:
        :return:
        """
        sample_n, feature_n = x.shape
        unused_features = np.ones(feature_n) == 1
        unclassified_samples = np.ones(sample_n) == 1
        self.node = self._train_and_build_tree(x, y, unused_features, unclassified_samples)
        return self

    def _train_and_build_tree(self, train_data, train_labels, unused_features, unclassified_samples):
        pass

    def _picking_which_attribute_to_split(self, x, y, unused_features, unclassified_samples):
        pass

    def predict(self, x):
        if self.node is None:
            raise Exception("predict not train")

        return self.node.predict(x)

    def draw(self, graph):
        """
        visualize decision tree nodes.
        :type graph: nx.DiGraph
        """
        root_label = "Root"
        graph.add_node(root_label)
        if self.node is None:
            raise Exception("label to draw not train")
        self.node.draw(graph, root_label)

    def export_to_json(self, json_tree):
        """

        :type json_tree: dict
        """
        json_tree["name"] = "root"
        json_tree["children"] = []
        current_children_list = json_tree["children"]
        self.node.export_to_json(current_children_list)


def prune(tree, alpha=0.1):
    """
    Prune decision tree.
    :type alpha: float
    :type tree: DecisionTree
    """
    node = tree.node
    if isinstance(node, DecisionTree.DecisionTreeLeafNode):
        return
    elif isinstance(node, DecisionTree.DecisionTreeClassifyNode):
        for sub_value in node.subtrees.values():
            if isinstance(sub_value, DecisionTree.DecisionTreeClassifyNode):
                prune_node(sub_value, tree, alpha)


def prune_node(node, tree, alpha):
    """
    Prune decision tree node.
    :type tree: DecisionTree
    :type node: DecisionTree.DecisionTreeLeafNode | DecisionTree.DecisionTreeClassifyNode
    """
    if isinstance(node, DecisionTree.DecisionTreeLeafNode):
        return
    elif isinstance(node, DecisionTree.DecisionTreeClassifyNode):
        for sub_value in node.subtrees.values():
            if isinstance(sub_value, DecisionTree.DecisionTreeClassifyNode):
                prune_node(sub_value, tree, alpha)
        # Now all the sub_values are leaf nodes
        if all(isinstance(t, DecisionTree.DecisionTreeLeafNode) for t in node.subtrees.values()):
            # print "all leaf node"
            current_all_leaf_subtrees = node.subtrees.values()  # type: list[DecisionTree.DecisionTreeLeafNode]
            merged_label = []
            merged_data = []
            for leaf in current_all_leaf_subtrees:
                merged_data += leaf.data
                for i in xrange(len(leaf.data)):
                    merged_label.append(leaf.label)
            if cal_split_shanon_entropy_sum(merged_label) <= (len(merged_label) - 1) * alpha:
                node = DecisionTree.DecisionTreeLeafNode(get_most_probably_label(merged_label),
                                                         merged_data)


def cal_split_shanon_entropy_sum(list_of_ys):
    sum_of_entropy = cal_shanon_entropy(list_of_ys)
    sum_of_entropy *= len(list_of_ys)
    return sum_of_entropy


def cal_shanon_entropy(y):
    """
    calculate shanon entropy.
    :param y:
    :type y: list| np.ndarray
    :return: shanon entropy
    :rtype float
    """
    counter = collections.Counter(y)
    label_sum = float(sum(counter.values()))
    entropy = 0.0
    for num in counter.values():
        p = num / label_sum
        entropy += - p * math.log(p, 2)
    logger.debug('entropy: labels:' + str(list(y)) + ':' + str(entropy))
    return entropy


def get_most_probably_label(y):
    """
    Get most probably label
    :param y: labels
    :return: most probably label
    """
    if len(y) == 0:
        return None
    counter = collections.Counter(y)
    return counter.most_common(1)[0][0]
