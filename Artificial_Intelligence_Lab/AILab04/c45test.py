from c45 import *
from datasethandle import *
import unittest
import numpy as np
import logging

logger = logging.getLogger('unit test')
logging.basicConfig(level=logging.DEBUG)


class TestForEntropy(unittest.TestCase):
    def test_entropy(self):
        self.assertEqual(cal_shanon_entropy(np.array(['a', 'a'])), 0)
        self.assertEqual(cal_shanon_entropy(np.array(['a', 'b'])), 1)


class TestGetMostCommonLabel(unittest.TestCase):
    def test_get_most_common_label(self):
        self.assertEqual(get_most_probably_label(np.array(['a', 'b', 'b', 'c', 'c', 'c'])), 'c')


class TestDecideTree(unittest.TestCase):
    def test_train_only_one_label(self):
        tree = C45DecisionTree()
        data = np.array([[0, 0], [0, 1], [1, 1]])
        labels = np.array(['a', 'a', 'a'])
        self.assertEqual(tree.fit(data, labels).node,
                         C45DecisionTree.DecisionTreeLeafNode('a', [[0, 0], [0, 1], [1, 1]]))

    def test_train_with_subtree(self):
        tree = C45DecisionTree()
        data = np.array([[0, 0], [1, 1], [2, 1]])
        labels = np.array(['a', 'b', 'c'])
        sub_tree = {0: C45DecisionTree.DecisionTreeLeafNode('a', [0, 0]),
                    1: C45DecisionTree.DecisionTreeLeafNode('b', [1, 1]),
                    2: C45DecisionTree.DecisionTreeLeafNode('c', [2, 2])}
        t = tree.fit(data, labels)
        self.assertEqual(t.node, C45DecisionTree.DecisionTreeClassifyNode(0, sub_tree))

    def test_train_classify(self):
        data = np.array([[1, 0, 0], [0, 0, 0], [0, 0, 1], [0, 1, 1]])
        labels = np.array([0, 1, 1, 2])
        tree = C45DecisionTree()
        tree.fit(data, labels)
        self.assertEqual(tree.predict([1, 1, 1]), 2)
        self.assertEqual(tree.predict([0, 0, 2]), 1)
        self.assertEqual(tree.predict([1, 0, 0]), 0)


class TestDataLoad(unittest.TestCase):
    def test_load_data_from_file(self):
        importer = Importer()
        infile_path = '/Users/wen/git/Lab/Artificial_Intelligence_Lab/AILab04/testdata.txt'
        data_matrix = importer.import_file_to_matrix(infile_path)
        logger.debug('loaded data matrix shape: ' + str(data_matrix.shape))
        self.assertEqual(data_matrix.shape[0], 75)

    def test_data_split(self):
        importer = Importer()
        infile_path = '/Users/wen/git/Lab/Artificial_Intelligence_Lab/AILab04/testdata.txt'
        data_matrix = importer.import_file_to_matrix(infile_path)
        x, y = importer.split_import_matrix_to_x_y()
        logger.debug('loaded data matrix of x shape: ' + str(x.shape))
        logger.debug('loaded data matrix of y shape: ' + str(y.shape))


if __name__ == '__main__':
    unittest.main()
