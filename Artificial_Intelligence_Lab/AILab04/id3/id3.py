from decision_tree import *

import numpy as np


class ID3DecisionTree(DecisionTree):
    def _train_and_build_tree(self, train_data, train_labels, unused_features, unclassified_samples):
        y = train_labels[unclassified_samples]
        if len(set(y)) == 1:
            return ID3DecisionTree.DecisionTreeLeafNode(y[0], train_data[unclassified_samples].tolist())

        min_feature, min_values, min_entropy = self._picking_which_attribute_to_split(train_data[unclassified_samples],
                                                                                      y, unused_features,
                                                                                      unclassified_samples)
        if min_feature is None:
            label = get_most_probably_label(y)
            data = []
            return ID3DecisionTree.DecisionTreeLeafNode(label, data)
        else:
            subtrees = {value: None for value in min_values}
            unused_features = unused_features.copy()
            unused_features[min_feature] = 0
            for value in min_values:
                value_unclassified_samples = unclassified_samples.copy()
                value_unclassified_samples[train_data[:, min_feature] != value] = 0
                # TODO: change != to two sub node 1)greater than value, 2)less than value
                subtrees[value] = self._train_and_build_tree(train_data, train_labels, unused_features,
                                                             value_unclassified_samples)

            return ID3DecisionTree.DecisionTreeClassifyNode(min_feature, subtrees)

    def _picking_which_attribute_to_split(self, x, y, unused_features, unclassified_samples, continuous=False):
        min_entropy, min_feature, min_values = np.inf, None, None

        for feature_index in range(len(unused_features)):
            if unused_features[feature_index] == 0:
                continue

            values = set(x[:, feature_index])
            entropy = 0.0
            if continuous:
                sorted_values = sorted(values)
                mid_values = [(sorted_values[i] + sorted_values[i + 1]) / 2.0 for i in xrange(len(sorted_values) - 1)]
                for value in mid_values:
                    pass
            else:
                for value in values:
                    x_index = (x[:, feature_index] == value)  # type: np.ndarray
                    y_value = y[x_index]
                    entropy += float(x_index.sum()) / unclassified_samples.sum() * cal_shanon_entropy(y_value)

            if entropy <= min_entropy:
                min_entropy = entropy
                min_feature = feature_index
                min_values = values

        return min_feature, min_values, min_entropy
