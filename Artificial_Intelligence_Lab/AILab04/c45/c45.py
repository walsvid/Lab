from decision_tree import *

import numpy as np
import math


class C45DecisionTree(DecisionTree):
    def _train_and_build_tree(self, train_data, train_labels, unused_features, unclassified_samples):
        y = train_labels[unclassified_samples]
        if len(set(y)) == 1:
            return C45DecisionTree.DecisionTreeLeafNode(y[0], train_data[unclassified_samples].tolist())

        min_feature, min_values, min_ratio = self._picking_which_attribute_to_split(train_data[unclassified_samples], y,
                                                                                    unused_features,
                                                                                    unclassified_samples)
        if min_feature is None:
            label = get_most_probably_label(y)
            data = []
            return C45DecisionTree.DecisionTreeLeafNode(label, data)
        else:
            subtrees = {value: None for value in min_values}
            unused_features = unused_features.copy()
            unused_features[min_feature] = 0
            for value in min_values:
                value_unclassified_samples = unclassified_samples.copy()
                # TODO: change != to two sub node 1)greater than value, 2)less than value
                value_unclassified_samples[train_data[:, min_feature] != value] = 0
                subtrees[value] = self._train_and_build_tree(train_data, train_labels, unused_features,
                                                             value_unclassified_samples)

            return C45DecisionTree.DecisionTreeClassifyNode(min_feature, subtrees)

    def _picking_which_attribute_to_split(self, x, y, unused_features, unclassified_samples):
        min_ratio, min_entropy, min_feature, min_values = np.inf, np.inf, None, None
        info = []
        for feature_index in range(len(unused_features)):
            if unused_features[feature_index] == 0:
                continue

            values = set(x[:, feature_index])
            entropy_d_a = 0.0
            split_info = 0.0

            for value in values:
                x_index = (x[:, feature_index] == value)  # type: np.ndarray
                y_value = y[x_index]
                d_j = float(x_index.sum())
                d = unclassified_samples.sum()
                entropy_d_a += d_j / d * cal_shanon_entropy(y_value)
                split_info -= d_j / d * math.log((d_j / d), 2)
            information_ratio = entropy_d_a / split_info
            info.append(information_ratio)

            if information_ratio <= min_ratio:
                min_ratio = information_ratio
                min_entropy = entropy_d_a
                min_feature = feature_index
                min_values = values

        return min_feature, min_values, min_ratio
