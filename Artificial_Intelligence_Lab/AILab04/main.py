from id3 import *
from c45 import *
from datasethandle import *
import numpy as np
import networkx as nx
import json


def main_id3():
    # Load train data from file
    infile_path = '/Users/wen/git/Lab/Artificial_Intelligence_Lab/AILab04/traindata.txt'
    importer = Importer()
    data_matrix = importer.import_file_to_matrix(infile_path)
    x, y = importer.split_import_matrix_to_x_y()

    tree = ID3DecisionTree()
    tree.fit(x, y)
    res1 = tree.predict([5.1, 3.5, 1.4, 0.2])
    assert res1 == 1
    res2 = tree.predict(np.array([7.1, 3, 5.9, 2.1]))
    assert res2 == 3
    # Load test data from file
    test_data_file_path = '/Users/wen/git/Lab/Artificial_Intelligence_Lab/AILab04/testdata.txt'
    test_importer = Importer()
    test_data_matrix = test_importer.import_file_to_matrix(test_data_file_path)
    test_x, test_y = test_importer.split_import_matrix_to_x_y()
    cnt = 0
    for n, test_item in enumerate(test_x):
        res = tree.predict(test_item)
        if res != test_y[n]:
            cnt += 1
    print "Correct rate using ID3: %0.2f%%" % ((1 - float(cnt) / len(test_y)) * 100)
    # graph = nx.DiGraph()
    # tree.draw(graph)
    # nx.write_gexf(graph, "id3.gexf")
    # tree_dict = {}
    # tree.export_to_json(tree_dict)
    # with open('id3.json', 'w') as f:
    #     json.dump(tree_dict, f)

    prune(tree)


def main_c45():
    # Load train data from file
    infile_path = '/Users/wen/git/Lab/Artificial_Intelligence_Lab/AILab04/traindata.txt'
    importer = Importer()
    data_matrix = importer.import_file_to_matrix(infile_path)
    x, y = importer.split_import_matrix_to_x_y()

    # Build Decision tree using c4.5
    tree = C45DecisionTree()
    tree.fit(x, y)

    # Load test data from file
    test_data_file_path = '/Users/wen/git/Lab/Artificial_Intelligence_Lab/AILab04/testdata.txt'
    test_importer = Importer()
    test_data_matrix = test_importer.import_file_to_matrix(test_data_file_path)
    test_x, test_y = test_importer.split_import_matrix_to_x_y()

    cnt = 0
    for n, test_item in enumerate(test_x):
        res = tree.predict(test_item)
        if res != test_y[n]:
            cnt += 1
    print "Correct rate using C4.5: %0.2f%%" % ((1 - float(cnt) / len(test_y)) * 100)
    # graph = nx.DiGraph()
    # tree.draw(graph)
    # nx.write_gexf(graph, "c45.gexf")
    # tree_dict = {}
    # tree.export_to_json(tree_dict)
    # with open('c45.json', 'w') as f:
    #     json.dump(tree_dict, f)


def main():
    while True:
        try:
            algo = raw_input("please input your algorithm:(id3/c45)")
            if algo == "id3":
                print "id3-id3"
                main_id3()
            elif algo == "c45":
                print "c45-c45"
                main_c45()
                print "ok"
            else:
                print "please enter correct algorithm."
                continue
        except EOFError:
            break

if __name__ == '__main__':
    main()
