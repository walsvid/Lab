import numpy as np


class Importer(object):
    def __init__(self):
        self.import_file = None
        self.import_matrix = None
        self.import_input = None
        self.import_x = None
        self.import_y = None

    def import_file_to_matrix(self, in_file):
        # type: (str) -> np.ndarray
        self.import_matrix = np.loadtxt(in_file)
        return self.import_matrix

    def import_input_to_file(self):
        pass

    def split_import_matrix_to_x_y(self):
        data_scale, feature_and_label_num_sum = self.import_matrix.shape

        self.import_x = np.hsplit(self.import_matrix, np.array([feature_and_label_num_sum - 1,
                                                                feature_and_label_num_sum]))[0]
        tmp_import_y = np.hsplit(self.import_matrix, np.array([feature_and_label_num_sum - 1,
                                                               feature_and_label_num_sum]))[1]
        self.import_y = tmp_import_y.reshape(data_scale)
        return self.import_x, self.import_y


class Exporter(object):
    def __init__(self):
        pass

    def export_tree_to_dot(self):
        pass

    def export_tree_to_plot(self):
        pass

    def export_dict_to_json(self, tree_dict):
        pass
