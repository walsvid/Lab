#!/usr/bin/python2
# coding: utf-8
"""Package Import"""
import numpy as np
import sklearn
import sklearn.datasets
from sklearn.cross_validation import train_test_split


class BPNN(object):
    def __init__(self, nn_hdim):
        np.random.seed(0)
        self.iris = sklearn.datasets.load_iris()
        self.X, self.X_test, self.y, self.y_test = train_test_split(self.iris.data, self.iris.target, train_size=0.5)
        self.num_example = len(self.X)
        self.nn_input_dim = 4
        self.nn_output_dim = 3
        self.alpha = 0.0003
        self.reg_lambda = 0.01
        self.W1 = np.random.randn(self.nn_input_dim, nn_hdim) / np.sqrt(self.nn_input_dim)
        self.b1 = np.zeros((1, nn_hdim))
        self.W2 = np.random.randn(nn_hdim, self.nn_output_dim) / np.sqrt(nn_hdim)
        self.b2 = np.zeros((1, self.nn_output_dim))
        self.model = {'W1': self.W1, 'b1': self.b1, 'W2': self.W2, 'b2': self.b2}

    def calculate_loss(self):
        self.W1, self.b1, self.W2, self.b2 = self.model['W1'], self.model['b1'], self.model['W2'], self.model['b2']
        z1 = self.X.dot(self.W1) + self.b1
        a1 = np.tanh(z1)
        z2 = a1.dot(self.W2) + self.b2
        exp_scores = np.exp(z2)
        y_pridict = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)

        # Calculate the loss
        correct_y_pridict = -np.log(y_pridict[range(self.num_example), self.y])
        data_loss = np.sum(correct_y_pridict)

        # Add regulatization term to loss (optional)
        data_loss += self.reg_lambda / 2 * (np.sum(np.square(self.W1)) + np.sum(np.square(self.W2)))
        loss = 1. / self.num_example * data_loss
        return loss

    def pridict(self):
        self.W1, self.b1, self.W2, self.b2 = self.model['W1'], self.model['b1'], self.model['W2'], self.model['b2']
        z1 = self.X_test.dot(self.W1) + self.b1
        a1 = np.tanh(z1)
        z2 = a1.dot(self.W2) + self.b2
        exp_scores = np.exp(z2)
        y_pridict = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)
        y_head = np.argmax(y_pridict, axis=1)
        i = 0
        l = len(y_head)
        for j in range(l):
            if self.y_test[j] != y_head[j]:
                i += 1
        print "Correct rate of test set：%f%%" % ((l - i + 0.0) / l * 100)
        return y_head

    def build_model(self, num_passes=1000, print_loss=False):
        for i in xrange(0, num_passes):
            # Forward propagation
            z1 = self.X.dot(self.W1) + self.b1
            a1 = np.tanh(z1)
            z2 = a1.dot(self.W2) + self.b2
            exp_scores = np.exp(z2)
            y_pridict = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)

            # Back propagation
            delta3 = y_pridict
            delta3[range(self.num_example), self.y] -= 1
            dW2 = (a1.T).dot(delta3)
            db2 = np.sum(delta3, axis=0, keepdims=True)
            delta2 = delta3.dot(self.W2.T) * (1 - np.power(a1, 2))
            dW1 = np.dot(self.X.T, delta2)
            db1 = np.sum(delta2, axis=0)

            # Regularization trems
            dW1 += self.reg_lambda * self.W1
            dW2 += self.reg_lambda * self.W2

            # Gradient descent parameter update
            self.W1 += -self.alpha * dW1
            self.b1 += -self.alpha * db1
            self.W2 += -self.alpha * dW2
            self.b2 += -self.alpha * db2

            # Assign value for model
            model = {'W1': self.W1, 'b1': self.b1, 'W2': self.W2, 'b2': self.b2}

            if print_loss and i % 100 == 0:
                print "Loss after iteration %i: %f" % (i, self.calculate_loss())
        return model
