#!/usr/bin/python2
# coding: utf-8
from bpnn import *


def main():
    model = []
    for i in xrange(10):
        model.append(BPNN(10))
        print "The %2d object ID：%d" % (i + 1, id(model[i]))
        model[i].build_model()
        model[i].pridict()


if __name__ == '__main__':
    main()
