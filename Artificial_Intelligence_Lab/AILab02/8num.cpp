#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define ALL_STATES 362880
#define CLOSED -1
#define NO 0
#define OPEN 1

typedef struct
{
    int arrange, preIndex, flag, f, g, h;
    char choice;
}State;

State state[ALL_STATES];

int start, goal, startIndex, goalIndex;

int arrangeToIndex(int arrange)
{
    int i, j, k, index = 0, t = 0, d[10];
    while (arrange)
    {
        d[++t] = arrange % 10;
        arrange /= 10;
    }
    for (i = t; i > 1; --i)
    {
        k = 0;
        for (j = 1; j < i; ++j)
        {
            if (d[i] > d[j])
                ++k;
        }
        index = index * i + k;
    }
    return index;
}

int heap[ALL_STATES + 4], heapIndex[ALL_STATES + 4], heapN;

void heapUp(int j)
{
    int i = j / 2, x = heap[j];
    while (i > 0)
    {
        if (state[heap[i]].f <= state[x].f) break;
        heapIndex[heap[j] = heap[i]] = j;
        j = i;
        i = j / 2;
    }
    heapIndex[heap[j] = x] = j;
}

void heapDown(int i)
{
    int j = i + i, x = heap[i];
    while (j <= heapN) {
        if ((j < heapN) && (state[heap[j]].f > state[heap[j + 1]].f)) ++j;
        if (state[x].f <= state[heap[j]].f) break;
        heapIndex[heap[i] = heap[j]] = i;
        i = j;
        j = i + i;
    }
    heapIndex[heap[i] = x] = i;
}

int heapPop()
{
    int x = heap[1];
    heapIndex[heap[1] = heap[heapN--]] = 1;
    if (heapN > 1)
        heapDown(1);
    return x;
}

void heapAdd(int x)
{
    ++heapN;
    heapIndex[heap[heapN] = x] = heapN;
    heapUp(heapN);
}

int calcH(int arrange)
{
    int p, i, h = 0;
    int map[] = {1, 2, 3, 8, 9, 4, 7, 6, 5};
    int map2[] = {0,1,2,5,8,7,6,3,4};
    for (i = 8; i >= 0; --i)
    {
        p = arrange % 10 - 1;
        arrange /= 10;
//        h += abs(p % 3 - i % 3) + abs(p / 3 - i / 3);
        h += abs(map2[p] - i % 3) + abs(map2[p] / 3 - i / 3);
//        h += p == map[i] ? 0 : 1;
    }
    return h;
}

int input()
{
    int ch, i;
    start = goal = 0;
    for (i = 0; i < 9; ++i)
    {
        do
        {
            ch = getchar();
        } while ((ch != EOF) && (('8' < ch) || (ch < '1')) && (ch != 'x'));

        if (ch == EOF)  return 0;
        if (ch == 'x')  start = start * 10 + 9;
        else            start = start * 10 + ch - '0';
//        goal = goal * 10 + i + 1;
    }
    goal = 123894765; // Change to another goal state.
    return 1;
}

int sameParity()
{
    int a = start, d[10], t = 0, sum = 0, i, j;
    while (a)
    {
        d[t++] = a % 10;
        a /= 10;
    }
    for (i = 1; i < t; ++i)
        for (j = 0; j < i; ++j)
            if ((d[i] > d[j]) && (d[i] != 9)) ++sum;
    return sum % 2;
}

char choice[4];
int nextIndex[4];
void next(int arrange)
{
    static int DI[] = { 0, 1, 0, -1 };
    static int DJ[] = { 1, 0, -1, 0 };
    static char DC[] = { 'l', 'u', 'r', 'd' };
    static int p[3][3];
    int i, j, i0, j0, x, y, k;
    for (i = 0; i < 3; ++i)
        for(j = 0; j < 3; ++j)
        {
            p[i][j] = arrange % 10;
            arrange /= 10;
            if (p[i][j] == 9)
            {
                i0 = i;
                j0 = j;
            }
        }

    for (k = 0; k < 4; ++k)
    {
        i = i0 + DI[k];
        j = j0 + DJ[k];
        if ((0 <= i) && (i < 3) && (0 <= j) && (j < 3) )
        {
            p[i0][j0] = p[i][j];
            p[i][j] = 9;
            arrange = 0;

            for (x = 2; x >= 0; --x)
                for (y = 2; y >= 0; --y)
                    arrange = arrange * 10 + p[x][y];
            p[i][j] = p[i0][j0];
            x = nextIndex[k] = arrangeToIndex(arrange);
            choice[k] = DC[k];
            if (state[x].arrange == 0)
            {
                state[x].arrange = arrange;
                state[x].h = calcH(arrange);
            }
        }
        else
        {
            nextIndex[k] = -1;
        }
    }
}

int astar()
{
    int i, j, k, ng, nf;
    if (!sameParity()) return 0;
    startIndex = arrangeToIndex(start);
    goalIndex = arrangeToIndex(goal);
    if (start == goal) return 1;

    memset(state, 0, sizeof(state));
    state[startIndex].arrange = start;
    state[startIndex].flag = OPEN;
    state[startIndex].g = 0;
    state[startIndex].h = state[startIndex].f = calcH(start);
    heapN = 0;
    heapAdd(startIndex);
    while (heapN > 0)
    {
        i = heapPop();
        if (i == goalIndex) return 1;
        state[i].flag = CLOSED;
        ng = state[i].g + 1;
        next(state[i].arrange);
        for (k = 0; k < 4; ++k)
        {
            j = nextIndex[k];
            if (j < 0) continue;
            nf = ng + state[j].h;
            if ((state[j].flag == NO) || ((state[j].flag == OPEN) && (nf < state[j].f)))
            {
                state[j].preIndex = i;
                state[j].choice = choice[k];
                state[j].g = ng;
                state[j].f = nf;
                if (state[j].flag == OPEN )
                {
                    heapUp(heapIndex[j]);
                    heapDown(heapIndex[j]);
                }
                else
                {
                    heapAdd(j);
                    state[j].flag = OPEN;
                }
            }
        }
    }
    return 0;
}

char* reformatArrange(int arrange, char dst[])
{
    for (int i = 8; i >= 0; --i)
    {
        dst[i] = (char) ((arrange % 10) + '0');
        if (dst[i] == '9') dst[i] = ' ';
        arrange /= 10;
    }
    return dst;
}

void output()
{
    int i, top = -1;
    char stack[1000][9];
    char movements[1000];
    for (i = goalIndex; i != startIndex; i = state[i].preIndex)
    {
        reformatArrange(state[i].arrange, stack[++top]);
        movements[top] = state[i].choice;
    }
    for (i = top; i >= 0; --i) {
        printf("Step %d:\n", top - i + 1);
        printf("Change blank with its %c number.\n", movements[i]);
        printf("%c %c %c\n%c %c %c\n%c %c %c", stack[i][0],stack[i][1],stack[i][2],
               stack[i][3],stack[i][4],stack[i][5],stack[i][6],stack[i][7],stack[i][8]);
        printf("\n");
    }

}


int main(int argc, char* argv[])
{
    while (input())
    {
        if (astar())
            output();
        else
            printf("Unsolvable\n\n");
    }
    return 0;
}
