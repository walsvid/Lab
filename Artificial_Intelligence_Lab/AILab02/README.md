# 简介
本次实验室为了解决8数码问题，属于搜索问题的一种。为了解决这一问题，使用`A*算法`进行搜索。其中的启发函数`h*()`使用`Manhattan Distance`。

# 用法
1. 编译生成可执行文件
    ```
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ mv AILab02 ../
    ```

2. 用Python脚本`shuffle.py`生成测试数据，储存在`test.txt`中。默认生成了一些测试数据。
    ```
    $ python shuffle.py
    ```

3. 利用输入输出的重定向
    ```
    $ ./AILab01 < test.txt > out.txt
    ```

4. Benchmark

    第1次1000次测试数据： 20.71s user 0.06s system 99% cpu 20.888 total
    
    第2次1000次测试数据： 20.75s user 0.07s system 99% cpu 20.924 total
    
    第3次10000次测试数据： 204.18s user 0.78s system 99% cpu 3:26.78 total

    目前这个算法的效率还有待提高。
    
    改进了h函数的求法，从原来的未在正确位置的数字的数目改成了到正确位置的曼哈顿距离。