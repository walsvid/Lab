"""
This module generate test data.

prerequisite:
    based on Python 2.x

usage: python shuffle.py

author: woonchao@163.com

"""
# encoding=UTF-8

from random import Random


def shuffle(li):
    """
    This function shuffled the list
    :param li: a list need to shuffle
    :return li: the list after shuffle
    """
    rand = Random()
    for x in xrange(len(li) - 1, 0, -1):
        y = rand.randint(0, x)
        li[x], li[y] = li[y], li[x]
    return li


def main():
    lis = ['1', '2', '3', '4', '5', '6', '7', '8', 'x']
    res = []
    for i in xrange(1000):
        shuffle(lis)
        res.append(' '.join(lis))
    fo = open("test.txt", "a")
    for i in res:
        fo.write(i)
        fo.write('\r\n')
    fo.close()


if __name__ == '__main__':
    import sys

    sys.exit(int(main() or 0))
