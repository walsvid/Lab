# coding: utf-8

import networkx as nx
import matplotlib.pyplot as plt
from Queue import Queue


class State:
    def __init__(self, m=3, c=3, b=1, max_people_num=3, d=0):
        self.m = m
        self.c = c
        self.b = b
        self.max_num = max_people_num
        self.father = None
        self.queue = Queue()
        self.depth = d
        self.visited = False

    def get_state_label(self):
        state_label = '' + str(self.m) + '-' + str(self.c) + '-' + str(self.b)
        return state_label

    def get_para(self):
        return self.m, self.c, self.b

    def set_father_state(self, father_state):
        self.father = father_state


def judge_state_validity(tmp_m, tmp_c, max=3):
    if 0 <= tmp_m <= max and 0 <= tmp_c <= max:
        if (tmp_m == tmp_c) or (tmp_m == max) or (tmp_m == 0):
            return True
        else:
            return False
    else:
        return False


def cross_river(state, m_on_boat, c_on_boat, b_current, visited_dict, back=False):
    if b_current == 1:
        m_tmp = state.m - m_on_boat
        c_tmp = state.c - c_on_boat
    else:
        m_tmp = state.m + m_on_boat
        c_tmp = state.c + c_on_boat

    if judge_state_validity(m_tmp, c_tmp, state.max_num):
        query_label = '' + str(m_tmp) + '-' + str(c_tmp) + '-' + str((b_current + 1) % 2)
        if query_label in visited_dict.keys():
            return visited_dict[query_label] if back is True else None
        else:
            next_state = State(m_tmp, c_tmp, (b_current + 1) % 2, state.max_num, state.depth + 1)
            return next_state
    else:
        return None


def generate_strategy(max_num_per_boat):
    strategy_list = []
    for i in range(max_num_per_boat + 1):
        for j in range(max_num_per_boat - i + 1):
            if 0 < i + j <= max_num_per_boat:
                strategy_list.append((i, j))
    return strategy_list


def search(init_m, init_c, init_b, init_s, init_max):
    has_solution = True
    graph = nx.DiGraph()
    strategy = generate_strategy(init_s)
    visited_state_dict = dict()
    current_state = State(init_m, init_c, init_b, init_max)
    root_state_label = current_state.get_state_label()
    print root_state_label
    graph.add_node(root_state_label, with_labels=True)
    visited_state_dict[root_state_label] = current_state
    while not (current_state.m == 0 and current_state.c == 0):
        for i, j in strategy:
            next_state = cross_river(current_state, i, j, current_state.b, visited_state_dict)
            if (next_state is not None) and next_state.depth > current_state.depth:
                graph.add_node(next_state.get_state_label())
                graph.add_edge(current_state.get_state_label(), next_state.get_state_label())
                visited_state_dict[next_state.get_state_label()] = next_state
                print next_state.get_state_label()
                current_state.queue.put(next_state)
                next_state.father = current_state
            else:
                continue
        if current_state.queue.empty():
            current_state = current_state.father
            if current_state is None:
                print "No solution"
                has_solution = False
                break
        else:
            current_state = current_state.queue.get_nowait()

    if has_solution:  # find all path
        while current_state.father is not None:
            current_state.visited = True
            father_depth = current_state.father.depth
            for uncle_state in visited_state_dict.values():
                if uncle_state.depth == father_depth and current_state.father is not uncle_state:
                    for i, j in strategy:
                        if current_state == cross_river(uncle_state, i, j, uncle_state.b, visited_state_dict, back=True):
                            graph.add_edge(uncle_state.get_state_label(), current_state.get_state_label())

            for brother_state in visited_state_dict.values():
                if brother_state.depth == current_state.depth and brother_state.visited is False:
                    current_state = brother_state
                    break
                else:
                    current_state = current_state.father
                    break

    pos = nx.shell_layout(graph)
    nx.draw_networkx_labels(graph, pos, font_size=6, font_family='sans-serif')
    nx.draw(graph, pos)
    plt.savefig("result.png")
    nx.write_gexf(graph, "result.gexf")


class Problem:
    def __init__(self):
        self.result = ""

    @staticmethod
    def user_input():
        problem_m = input("Please enter the number of missionary:")
        problem_c = input("Please enter the number of cannibal:")
        problem_b = input("Please enter the initial state of boat('1' is left, '0' is right, default:1):")
        problem_s = input("Please enter the maximum number of people the boat can take:")
        return problem_m, problem_c, problem_b, problem_s, max(problem_m, problem_c)

    @classmethod
    def solve(cls):
        args = cls.user_input()
        try:
            search(*args)
            cls.result = "search function runs ok."
        except TypeError:
            cls.result = "the args for search function have wrong type."
        finally:
            print cls.result


def main():
    problem = Problem()
    problem.solve()

if __name__ == '__main__':
    main()
