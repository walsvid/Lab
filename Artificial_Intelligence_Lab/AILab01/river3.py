# coding: utf-8

import operator
import networkx as nx
import matplotlib.pyplot as plt
from Queue import Queue


class Tools:
    def __init__(self):
        pass

    @staticmethod
    def judge_state_validity(tmp_m, tmp_c, max_num):
        if 0 <= tmp_m <= max_num and 0 <= tmp_c <= max_num:
            if tmp_m == tmp_c or tmp_m == max_num or tmp_m == 0:
                return True
            else:
                return False
        else:
            return False

    @staticmethod
    def join_para_to_lable(*paras):
        return '-'.join([str(para) for para in paras])

    @staticmethod
    def change_to_next_state(state, m_on_boat, c_on_boat, visited_dict, back=False):
        op = operator.sub if state.get_b() == 1 else operator.add
        m_next_state = op(state.get_m(), m_on_boat)
        c_next_state = op(state.get_c(), c_on_boat)

        if Tools.judge_state_validity(m_next_state, c_next_state, state.get_max_num()):
            query_label = Tools.join_para_to_lable(m_next_state, c_next_state, (state.get_b() + 1) % 2)

            if query_label in visited_dict.keys():
                return visited_dict[query_label] if back is True else None
            else:
                next_state = State(m_next_state, c_next_state, (state.get_b() + 1) % 2,
                                   state.get_max_num(), state.get_depth() + 1)
                return next_state
        else:
            return None

    @staticmethod
    def generate_strategy(max_num_per_boat, strategy_list=None):
        if strategy_list is None:
            strategy_list = []
        for i in range(max_num_per_boat + 1):
            for j in range(max_num_per_boat - i + 1):
                if 0 < i + j <= max_num_per_boat:
                    strategy_list.append((i, j))
        return strategy_list

    @staticmethod
    def output(graph):
        pos = nx.shell_layout(graph)
        nx.draw_networkx_labels(graph, pos, font_size=6, font_family='sans-serif')
        nx.draw(graph, pos)
        plt.savefig("result.png")
        nx.write_gexf(graph, "result.gexf")


class State:
    def __init__(self, m=3, c=3, b=1, max_people_num=3, d=0):
        self.m = m
        self.c = c
        self.b = b
        self.max_num = max_people_num
        self.father = None
        self.queue = Queue()
        self.depth = d
        self.visited = False

    def get_state_label(self):
        state_label = Tools.join_para_to_lable(self.m, self.c, self.b)
        return state_label

    def get_m(self):
        return self.m

    def get_c(self):
        return self.c

    def get_b(self):
        return self.b

    def get_depth(self):
        return self.depth

    def get_max_num(self):
        return self.max_num

    def get_father(self):
        return self.father

    def set_m(self, new_m):
        self.m = new_m

    def set_c(self, new_c):
        self.c = new_c

    def set_b(self, new_b):
        self.b = new_b

    def set_max_num(self, new_max_num):
        self.max_num = new_max_num

    def set_father_state(self, new_father_state):
        self.father = new_father_state

    def is_visited(self):
        return self.visited


class Search:
    def __init__(self, init_m_, init_c_, init_b_, init_s_, init_max_):
        self.init_m = init_m_
        self.init_c = init_c_
        self.init_b = init_b_
        self.init_s = init_s_
        self.init_max = init_max_
        self.has_solution = True
        self.graph = nx.DiGraph()
        self.visited_state_dict = dict()
        self.strategy = []
        Tools.generate_strategy(self.init_s, self.strategy)
        self.current_state = State(self.init_m, self.init_c, self.init_b, self.init_max)

    def initial_graph(self):
        root_state_label = self.current_state.get_state_label()
        print root_state_label
        self.graph.add_node(root_state_label, with_labels=True)
        self.visited_state_dict[root_state_label] = self.current_state

    def choose_which_next_state_to_be_current_state(self):
        if self.current_state.queue.empty():
            if self.current_state.get_father() is None:
                print "No solution"
                self.has_solution = False
                return None
            return self.current_state.get_father()
        else:
            return self.current_state.queue.get_nowait()

    def bfs(self):
        while not (self.current_state.get_m() == 0 and self.current_state.get_c() == 0):
            for i, j in self.strategy:
                next_state = Tools.change_to_next_state(self.current_state, i, j, self.visited_state_dict)
                if (next_state is not None) and next_state.depth > self.current_state.depth:
                    self.graph.add_node(next_state.get_state_label())
                    self.graph.add_edge(self.current_state.get_state_label(), next_state.get_state_label())
                    self.visited_state_dict[next_state.get_state_label()] = next_state
                    print next_state.get_state_label()
                    self.current_state.queue.put(next_state)
                    next_state.father = self.current_state
                else:
                    continue
            self.current_state = self.choose_which_next_state_to_be_current_state()
            if self.current_state is None:
                break

    def add_edge_from_uncle_to_son(self):
        while self.current_state.get_father() is not None:
            self.current_state.visited = True
            father_depth = self.current_state.get_father().get_depth()
            for uncle_state in self.visited_state_dict.values():
                if uncle_state.get_depth() == father_depth and self.current_state.get_father() is not uncle_state:
                    for i, j in self.strategy:
                        if self.current_state == Tools.change_to_next_state(uncle_state, i, j, self.visited_state_dict,
                                                                            back=True):
                            self.graph.add_edge(uncle_state.get_state_label(), self.current_state.get_state_label())

            for brother_state in self.visited_state_dict.values():
                if brother_state.get_depth() == self.current_state.get_depth() and brother_state.is_visited() is False:
                    self.current_state = brother_state
                    break
                else:
                    self.current_state = self.current_state.get_father()
                    break

    def search_routine(self):
        self.initial_graph()
        self.bfs()
        if self.has_solution:
            self.add_edge_from_uncle_to_son()
        Tools.output(self.graph)


class Problem:
    def __init__(self):
        self.result = "problem has not been solved."

    def get_result(self):
        return self.result

    def set_result(self, string):
        self.result = string

    @staticmethod
    def input_parameter():
        problem_m = input("Please enter the number of missionary:")
        problem_c = input("Please enter the number of cannibal:")
        problem_b = input("Please enter the initial state of boat('1' is left, '0' is right, default:1):")
        problem_s = input("Please enter the maximum number of people the boat can take:")
        return problem_m, problem_c, problem_b, problem_s, min(problem_m, problem_c)

    def solve(self):
        args = self.input_parameter()
        try:
            search = Search(*args)
            search.search_routine()
            self.set_result("search function runs ok.")
        except TypeError:
            self.set_result("the args for search function have wrong type.")
        finally:
            print self.get_result()


def main():
    problem = Problem()
    problem.solve()


if __name__ == '__main__':
    main()
