# coding: utf-8

import networkx as nx
import matplotlib.pyplot as plt
from Queue import Queue


class State:
    def __init__(self, m=3, c=3, b=1, max_people_num=3):
        self.m = m
        self.c = c
        self.b = b
        self.max_num = max_people_num
        self.father = None
        self.queue = Queue()

    def get_state_label(self):
        state_label = '' + str(self.m) + '-' + str(self.c) + '-' + str(self.b)
        return state_label

    def get_para(self):
        return self.m, self.c, self.b

    def set_father_state(self, father_state):
        self.father = father_state


def judge_state_validity(tmp_m, tmp_c, max=3):
    if 0 <= tmp_m <= max and 0 <= tmp_c <= max:
        if (tmp_m == tmp_c) or (tmp_m == max) or (tmp_m == 0):
            return True
        else:
            return False
    else:
        return False


def cross_river(state, m_on_boat, c_on_boat, b_current):
    if b_current == 1:
        m_tmp = state.m - m_on_boat
        c_tmp = state.c - c_on_boat
    else:
        m_tmp = state.m + m_on_boat
        c_tmp = state.c + c_on_boat

    if judge_state_validity(m_tmp, c_tmp, state.max_num):
        next_state = State(m_tmp, c_tmp, (b_current + 1) % 2, state.max_num)
        return next_state
    else:
        return None


def generate_strategy(max_num_per_boat):
    strategy_list = []
    for i in range(max_num_per_boat + 1):
        for j in range(max_num_per_boat - i + 1):
            if 0 < i + j <= max_num_per_boat:
                strategy_list.append((i, j))
    return strategy_list


def search(init_m, init_c, init_b, init_s, init_max):

    graph = nx.DiGraph()
    strategy = generate_strategy(init_s)
    visited_state_list = []
    current_state = State(init_m, init_c, init_b, init_max)

    root_state_label = current_state.get_state_label()
    print root_state_label
    graph.add_node(root_state_label, with_labels=True)
    visited_state_list.append(root_state_label)

    while not (current_state.m == 0 and current_state.c == 0):
        for i, j in strategy:
            next_state = cross_river(current_state, i, j, current_state.b)
            if (next_state is not None) and next_state.get_state_label() not in visited_state_list:

                graph.add_node(next_state.get_state_label())
                graph.add_edge(current_state.get_state_label(), next_state.get_state_label())
                visited_state_list.append(next_state.get_state_label())

                print next_state.get_state_label()
                current_state.queue.put(next_state)
                next_state.father = current_state

            else:
                continue

        if current_state.queue.empty():
            current_state = current_state.father
            if current_state is None:
                print "No solution"
                break

        else:
            current_state = current_state.queue.get_nowait()

    pos = nx.shell_layout(graph)
    nx.draw_networkx_labels(graph, pos, font_size=6, font_family='sans-serif')
    nx.draw(graph, pos)
    plt.savefig("cross_river.png")
    nx.write_gexf(graph, "cross_river.gexf")


class Problem:
    def __init__(self):
        pass

    @staticmethod
    def user_input():
        problem_m = input("Please enter the number of missionary:")
        problem_c = input("Please enter the number of cannibal:")
        problem_b = input("Please enter the initial state of boat('1' is left, '0' is right, default:1):")
        problem_s = input("Please enter the maximum number of people on the boat:")
        return problem_m, problem_c, problem_b, problem_s, max(problem_m, problem_c)

    @classmethod
    def solve(cls):
        args = cls.user_input()
        search(*args)


def main():
    problem = Problem()
    problem.solve()

if __name__ == '__main__':
    main()
