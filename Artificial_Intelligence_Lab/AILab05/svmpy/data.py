import numpy as np


class DataImporter(object):
    def __init__(self, train_file="./data/traindata.txt", test_file="./data/testdata.txt"):
        self.train = np.hsplit(np.loadtxt(train_file), np.array([0, 9]))
        self.test = np.hsplit(np.loadtxt(test_file), np.array([0, 9]))
        self.train_sample = self.train[1]
        self.train_label = 2 * self.train[2] - 3
        self.test_sample = self.test[1]
        self.test_label = 2 * self.test[2] - 3

    def get_train_sample(self):
        return self.train_sample

    def get_train_label(self):
        return self.train_label

    def get_test_sample(self):
        return self.test_sample

    def get_test_label(self):
        return self.test_label

