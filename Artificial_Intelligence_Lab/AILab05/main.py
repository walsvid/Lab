# coding=utf-8
import svmpy
import logging
import argh
from collections import Counter


def example(data=svmpy.DataImporter()):
    """
    :type data: svmpy.DataImporter
    """
    samples = data.get_train_sample()
    labels = data.get_train_label()
    trainer = svmpy.SVMTrainer(svmpy.Kernel.gaussian(0.3), 0.1)
    predictor = trainer.train(samples, labels)
    test_samples = data.get_test_sample()
    test_labels = data.get_test_label()
    cnt = Counter()
    for i, j in zip(test_samples, test_labels):
        cnt[int(predictor.predict(i) - j)] += 1
    print 'Correct rate:'
    print format(float(cnt[0]) / test_labels.shape[0], '0.4%')


if __name__ == "__main__":
    logging.basicConfig(level=logging.ERROR)
    argh.dispatch_command(example)
